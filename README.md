# MovieGeeks

Movie Geeks Front End Project

## How to run locally

- Run `yarn start`
- Open `http://localhost:3000/` in your browser of preference

## How to run tests

Tests run with https://www.cypress.io/. Head to their website docs for more information

- Run `yarn start:test`. This will start the CypressUI and there you can choose which tests to run using the interactive UI

## Running the production build locally

- Run `yarn build`
- Run `yarn start:prod`
