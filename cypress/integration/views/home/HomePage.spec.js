/* eslint-disable */
/// <reference types="cypress" />
import { localStorageUtil } from '../../../../src/shared/util'

describe('Home Page', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('should render the banner', () => {
    cy.get('[data-testid=home-page]').find('[data-testid=home-banner]')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-banner]')
      .contains('Your minimalist online movie catalog')
  })

  it('should render the playing now section', () => {
    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active')
      .should('have.length', 4)

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:first')
      .contains('Venom')
  })

  it('should handle errors on the playing now section', () => {
    localStorageUtil.setTestResponseMode('error')
    cy.visit('/')
    cy.get('[data-testid=home-page]')
      .find('[data-testid=error-message-container]')
      .contains('An unexpected error has occurred')
  })

  it('should render the info panel', () => {
    cy.get('[data-testid=home-info-panel]').contains(
      'Track and review your favorite movies',
    )
    cy.get('[data-testid=home-info-panel]').contains(
      'Track everything you want to see by adding movies to your watchlist',
    )
  })

  it('should render the playing now section when logged in', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:first')
      .contains('Venom')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:first')
      .find('[data-testid=remove-from-watchlist-button]')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:last')
      .find('[data-testid=add-to-watchlist-button]')
  })

  it('should render ratings stuff', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:first')
      .find('[data-testid=slider-movie-rating]')
      .contains('Add Your Rating')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:first')
      .find('[data-testid=slider-movie-rating]')
      .find('.rc-slider')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('[data-testid=346910]')
      .find('[data-testid=slider-movie-rating]')
      .contains('Your Rating 8')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('[data-testid=346910]')
      .find('[data-testid=slider-movie-rating]')
      .find('[data-testid=remove-rating-button]')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('[data-testid=346910]')
      .find('[data-testid=slider-movie-rating]')
      .find('.rc-slider')
  })

  it('should redirect user to signup page if clicking addToWatchlist but not logged in', () => {
    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-slide:last')
      .find('[data-testid=add-to-watchlist-button]')
      .click()

    cy.url().should('include', 'signup')
  })

  it('should handle remove rating', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active')
      .find('[data-testid=346910]')
      .find('[data-testid=remove-rating-button]')
      .click()

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('[data-testid=346910]')
      .find('[data-testid=slider-movie-rating]')
      .contains('Add Your Rating')
  })

  it('should handle rating of a movie', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:first')
      .find('[data-testid=slider-movie-rating]')
      .find('.rc-slider')
      .click()

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:first')
      .find('[data-testid=slider-movie-rating]')
      .contains('Your Rating 5')
  })

  it('should handle addToWatchlist', () => {
    localStorageUtil.setTestIsLoggedIn(true)

    cy.visit('/')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:last')
      .find('[data-testid=add-to-watchlist-button]')
      .click()

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:last')
      .find('[data-testid=remove-from-watchlist-button]')
  })

  it('should handle removeFromWatchlist', () => {
    localStorageUtil.setTestIsLoggedIn(true)

    cy.visit('/')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:first')
      .find('[data-testid=remove-from-watchlist-button]')
      .click()

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:first')
      .find('[data-testid=add-to-watchlist-button]')
  })

  it('should handle errors when opening a movie trailer', () => {
    localStorageUtil.setTestResponseMode('error')

    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:first')
      .find('[data-testid=movie-card-top-actions]')
      .find('[data-testid=trailer-button]')
      .click()
  })

  it('should show movie trailers', () => {
    cy.get('[data-testid=home-page]')
      .find('[data-testid=home-playing-now]')
      .find('.slick-active:first')
      .find('[data-testid=movie-card-top-actions]')
      .find('[data-testid=trailer-button]')
      .click()

    cy.get('body')
      .find('.MuiDialog-container')
      .find('iframe')
  })
})
