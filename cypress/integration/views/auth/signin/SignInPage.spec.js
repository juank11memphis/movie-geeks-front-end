/* eslint-disable */
/// <reference types="cypress" />
import { localStorageUtil } from '../../../../../src/shared/util'

describe('SignIn Page', () => {
  beforeEach(() => {
    cy.visit('/signin')
  })

  it('should render properly', () => {
    cy.get('[data-testid=signin-page]')
      .find('h3')
      .contains('Sign In')

    cy.get('[data-testid=signin-page]')
      .find('button')
      .contains('Sign In with Google')

    cy.get('[data-testid=signin-page]')
      .find('span')
      .contains('Or use your email')

    cy.get('[data-testid=signin-page]').find('[data-testid=signin-form]')

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-form]')
      .find('[data-testid=field-email]')

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-form]')
      .find('[data-testid=field-password]')

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-button]')
      .contains('Sign In')

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=forgot-password-link]')
      .contains('I have forgotten my password')

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=new-to-moviegeeks]')
      .contains('New to MovieGeeks')

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signup-link]')
      .contains('Sign Up')
  })

  it('should show is required errors', () => {
    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-button]')
      .contains('Sign In')
      .click()

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=error-message-container]')
      .contains('Your email is required')

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=error-message-container]')
      .contains('Your password is required')
  })

  it('should show is invalid email error', () => {
    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-form]')
      .find('[data-testid=field-email] input')
      .type('invalidemail')

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-button]')
      .contains('Sign In')
      .click()

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=error-message-container]')
      .contains('Invalid email')
  })

  it('should show too short password error', () => {
    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-form]')
      .find('[data-testid=field-password] input')
      .type('short')

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-button]')
      .contains('Sign In')
      .click()

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=error-message-container]')
      .contains('Password must have at least 6 characters')
  })

  it('should sign in successfully', () => {
    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-form]')
      .find('[data-testid=field-email] input')
      .type('good@email.com')

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-form]')
      .find('[data-testid=field-password] input')
      .type('good password')

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-button]')
      .contains('Sign In')
      .click()

    cy.get('[data-testid=home-page]')
  })

  it('should handle errors on signin', () => {
    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-form]')
      .find('[data-testid=field-email] input')
      .type('good@email.com')

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-form]')
      .find('[data-testid=field-password] input')
      .type('good password')

    localStorageUtil.setTestResponseMode('error')

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signin-button]')
      .contains('Sign In')
      .click()

    cy.get('[data-testid=signin-page]')
      .find('[data-testid=error-message-container]')
      .contains('Mock Server Error')
  })

  it('should redirect to the Forgot Password Page', () => {
    cy.get('[data-testid=signin-page]')
      .find('[data-testid=forgot-password-link]')
      .contains('I have forgotten my password')
      .click()

    cy.url().should('include', '/forgot-password')
    cy.get('[data-testid=forgot-password-page]')
  })

  it('should redirect to the Sign Up Page', () => {
    cy.get('[data-testid=signin-page]')
      .find('[data-testid=signup-link]')
      .contains('Sign Up')
      .click()

    cy.url().should('include', '/signup')
    cy.get('[data-testid=signup-page]')
  })
})
