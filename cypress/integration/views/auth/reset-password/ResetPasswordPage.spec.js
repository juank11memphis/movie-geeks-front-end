/* eslint-disable */
/// <reference types="cypress" />
import { localStorageUtil } from '../../../../../src/shared/util'

describe('Reset Password Page', () => {
  beforeEach(() => {
    cy.visit('/reset-password')
  })

  it('should render properly', () => {
    cy.get('[data-testid=reset-password-page]')
      .find('h3')
      .contains('Enter your new password')

    cy.get('[data-testid=reset-password-page]')
      .find('button')
      .contains('Reset Password')

    cy.get('[data-testid=reset-password-page]')
      .find('[data-testid=reset-password-form]')
      .find('[data-testid=field-password]')
  })

  it('should show is required errors', () => {
    cy.get('[data-testid=reset-password-page]')
      .find('[data-testid=reset-password-button]')
      .contains('Reset Password')
      .click()

    cy.get('[data-testid=reset-password-page]')
      .find('[data-testid=error-message-container]')
      .contains('Your password is required')
  })

  it('should show password too short error', () => {
    cy.get('[data-testid=reset-password-page]')
      .find('[data-testid=reset-password-form]')
      .find('[data-testid=field-password] input')
      .type('short')

    cy.get('[data-testid=reset-password-page]')
      .find('[data-testid=reset-password-button]')
      .contains('Reset Password')
      .click()

    cy.get('[data-testid=reset-password-page]')
      .find('[data-testid=error-message-container]')
      .contains('Password must have at least 6 characters')
  })

  it('should Reset Password successfully', () => {
    cy.get('[data-testid=reset-password-page]')
      .find('[data-testid=reset-password-form]')
      .find('[data-testid=field-password] input')
      .type('good-new-password')

    cy.get('[data-testid=reset-password-page]')
      .find('[data-testid=reset-password-button]')
      .contains('Reset Password')
      .click()

    cy.get('[data-testid=home-page]')
  })

  it('should handle errors on Reset Password', () => {
    cy.get('[data-testid=reset-password-page]')
      .find('[data-testid=reset-password-form]')
      .find('[data-testid=field-password] input')
      .type('good-new-password')

    localStorageUtil.setTestResponseMode('error')

    cy.get('[data-testid=reset-password-page]')
      .find('[data-testid=reset-password-button]')
      .contains('Reset Password')
      .click()

    cy.get('[data-testid=reset-password-page]')
      .find('[data-testid=error-message-container]')
      .contains('Mock Server Error')
  })
})
