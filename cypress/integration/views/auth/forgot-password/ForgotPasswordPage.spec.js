/* eslint-disable */
/// <reference types="cypress" />
import { localStorageUtil } from '../../../../../src/shared/util'

describe('Forgot Password Page', () => {
  beforeEach(() => {
    cy.visit('/forgot-password')
  })

  it('should render properly', () => {
    cy.get('[data-testid=forgot-password-page]')
      .find('h3')
      .contains('Enter your email')

    cy.get('[data-testid=forgot-password-page]')
      .find('button')
      .contains('Request Password Reset')

    cy.get('[data-testid=forgot-password-page]')
      .find('[data-testid=forgot-password-form]')
      .find('[data-testid=field-email]')
  })

  it('should show is required errors', () => {
    cy.get('[data-testid=forgot-password-page]')
      .find('[data-testid=forgot-password-button]')
      .contains('Request Password Reset')
      .click()

    cy.get('[data-testid=forgot-password-page]')
      .find('[data-testid=error-message-container]')
      .contains('Your email is required')
  })

  it('should show is invalid email error', () => {
    cy.get('[data-testid=forgot-password-page]')
      .find('[data-testid=forgot-password-form]')
      .find('[data-testid=field-email] input')
      .type('invalidemail')

    cy.get('[data-testid=forgot-password-page]')
      .find('[data-testid=forgot-password-button]')
      .contains('Request Password Reset')
      .click()

    cy.get('[data-testid=forgot-password-page]')
      .find('[data-testid=error-message-container]')
      .contains('Invalid email')
  })

  it('should request password reset successfully', () => {
    cy.get('[data-testid=forgot-password-page]')
      .find('[data-testid=forgot-password-form]')
      .find('[data-testid=field-email] input')
      .type('good@email.com')

    cy.get('[data-testid=forgot-password-page]')
      .find('[data-testid=forgot-password-button]')
      .contains('Request Password Reset')
      .click()

    cy.get('[data-testid=forgot-password-page]')
      .find('h3')
      .contains('Check your email for further instructions')
  })

  it('should handle errors on request password reset', () => {
    cy.get('[data-testid=forgot-password-page]')
      .find('[data-testid=forgot-password-form]')
      .find('[data-testid=field-email] input')
      .type('good@email.com')

    localStorageUtil.setTestResponseMode('error')

    cy.get('[data-testid=forgot-password-page]')
      .find('[data-testid=forgot-password-button]')
      .contains('Request Password Reset')
      .click()

    cy.get('[data-testid=forgot-password-page]')
      .find('[data-testid=error-message-container]')
      .contains('Mock Server Error')
  })
})
