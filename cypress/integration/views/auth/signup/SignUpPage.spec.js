/* eslint-disable */
/// <reference types="cypress" />
import { localStorageUtil } from '../../../../../src/shared/util'

describe('SignUp Page', () => {
  beforeEach(() => {
    cy.visit('/signup')
  })

  it('should render properly', () => {
    cy.get('[data-testid=signup-page]')
      .find('h3')
      .contains('Sign Up')

    cy.get('[data-testid=signup-page]')
      .find('button')
      .contains('Sign Up with Google')

    cy.get('[data-testid=signup-page]')
      .find('span')
      .contains('Or use your email')

    cy.get('[data-testid=signup-page]').find('[data-testid=signup-form]')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-firstname]')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-lastname]')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-email]')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-password]')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-button]')
      .contains('Sign Up')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=already-member]')
      .contains('Already a member?')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signin-link]')
      .contains('Sign In')
  })

  it('should show is required errors', () => {
    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-button]')
      .contains('Sign Up')
      .click()

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=error-message-container]')
      .contains('Your first name is required')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=error-message-container]')
      .contains('Your last name is required')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=error-message-container]')
      .contains('Your email is required')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=error-message-container]')
      .contains('Your password is required')
  })

  it('should show is invalid email error', () => {
    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-email] input')
      .type('invalidemail')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-button]')
      .contains('Sign Up')
      .click()

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=error-message-container]')
      .contains('Invalid email')
  })

  it('should show too short password error', () => {
    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-password] input')
      .type('short')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-button]')
      .contains('Sign Up')
      .click()

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=error-message-container]')
      .contains('Password must have at least 6 characters')
  })

  it('should sing up successfully', () => {
    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-firstname] input')
      .type('goodfirstname')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-lastname] input')
      .type('goodlastname')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-email] input')
      .type('good@email.com')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-password] input')
      .type('good password')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-button]')
      .contains('Sign Up')
      .click()

    cy.get('[data-testid=home-page]')
  })

  it('should handle errors on sign up', () => {
    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-firstname] input')
      .type('goodfirstname')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-lastname] input')
      .type('goodlastname')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-email] input')
      .type('good@email.com')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-form]')
      .find('[data-testid=field-password] input')
      .type('good password')

    localStorageUtil.setTestResponseMode('error')

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signup-button]')
      .contains('Sign Up')
      .click()

    cy.get('[data-testid=signup-page]')
      .find('[data-testid=error-message-container]')
      .contains('Mock Server Error')
  })

  it('should redirect to the Sign In Page', () => {
    cy.get('[data-testid=signup-page]')
      .find('[data-testid=signin-link]')
      .contains('Sign In')
      .click()

    cy.url().should('include', '/signin')
    cy.get('[data-testid=signin-page]')
  })
})
