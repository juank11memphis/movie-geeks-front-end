/* eslint-disable */
/// <reference types="cypress" />
import { localStorageUtil } from '../../../../src/shared/util'

const loadData = (hasNext = true) => ({
  userMovies: [
    {
      id: 665544,
      userId: '11',
      movieId: '346910',
      rating: 7,
      saved: false,
      movie: {
        id: 346910,
        title: 'The Predator',
        posterPath: 'wMq9kQXTeQCHUZOG4fAe5cAxyUA.jpg',
        releaseDate: '2018-09-14',
      },
    },
    {
      id: 665544,
      userId: '11',
      movieId: '346910',
      rating: 7,
      saved: false,
      movie: {
        id: 346910,
        title: 'The Predator 2',
        posterPath: 'wMq9kQXTeQCHUZOG4fAe5cAxyUA.jpg',
        releaseDate: '2018-09-14',
      },
    },
  ],
  pagingMetadata: { next: 'dsadasdsadsa', hasNext },
})

describe('Profile Page Filters', () => {
  beforeEach(() => {
    localStorageUtil.setTestIsLoggedIn(true)
  })

  it('should load paginated results in watchlist', () => {
    localStorageUtil.setTestServiceDataOverride([
      {
        service: 'loadWatchlist',
        data: loadData(),
      },
    ])
    cy.visit('/profile/11/watchlist')
    cy.wait(500)
    cy.scrollTo(0, 1000)

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]')
      .should('have.length', 4)
  })

  it('should load paginated results in collection', () => {
    localStorageUtil.setTestServiceDataOverride([
      {
        service: 'loadCollection',
        data: loadData(),
      },
    ])
    cy.visit('/profile/11/collection')
    cy.wait(500)
    cy.scrollTo(0, 1000)

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]')
      .should('have.length', 4)
  })
})
