/* eslint-disable */
/// <reference types="cypress" />
import { localStorageUtil } from '../../../../src/shared/util'

describe('Profile Page', () => {
  beforeEach(() => {
    cy.visit('/profile/11/watchlist')
  })

  it('should render properly when there is no data returned', () => {
    localStorageUtil.setTestResponseMode('no-data')

    cy.visit('/profile/11/watchlist')

    cy.get('[data-testid=profile-page]')
      .find('h5')
      .contains('No movies in watchlist')
  })

  it('should render properly when there is data and user is not logged in', () => {
    cy.get('[data-testid=profile-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]')
      .should('have.length', 2)
  })

  it('should render properly when there is data and user is logged in', () => {
    localStorageUtil.setTestIsLoggedIn(true)

    cy.visit('/profile/11/watchlist')

    cy.get('[data-testid=profile-page]').find('[data-testid=filters-container]')

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]')
      .should('have.length', 2)

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]:first')
      .find('[data-testid=remove-from-watchlist-button]')

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=not-me-user-text]')
      .should('have.length', 0)
  })

  it('should handle errors loading profile watchlist', () => {
    localStorageUtil.setTestResponseMode('error')

    cy.visit('/profile/11/watchlist')

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=error-message-container]')
      .contains('An unexpected error has occurred')
  })

  it('should handle errors loading profile collection', () => {
    localStorageUtil.setTestResponseMode('error')

    cy.visit('/profile/11/collection')

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=error-message-container]')
      .contains('An unexpected error has occurred')
  })

  it('should render profile collection when user has no movies', () => {
    localStorageUtil.setTestResponseMode('no-data')
    cy.visit('/profile/11/collection')

    cy.get('[data-testid=profile-page]')
      .find('h5')
      .contains('No movies in collection')
  })

  it('should render profile collection when user is not logged in', () => {
    cy.visit('/profile/11/collection')

    cy.get('[data-testid=profile-page]')
      .find('h2')
      .contains('Juan Morales')

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]')
      .should('have.length', 1)

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]:first')
      .find('[data-testid=slider-movie-rating]')
      .contains('Add Your Rating')

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=profile-page-header]')
      .should('have.length', 0)
  })

  it('should render profile collection when user is logged in and seeing his own collection', () => {
    localStorageUtil.setTestIsLoggedIn(true)

    cy.visit('/profile/11/collection')

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]')
      .should('have.length', 1)

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]:first')
      .find('[data-testid=slider-movie-rating]')
      .contains('Your Rating 8')
  })

  it('should render profile collection when user is logged in and seeing someone else collection', () => {
    const loadCollectionData = {
      userMovies: [
        {
          id: 665544,
          userId: '11',
          movieId: '346910',
          rating: 7,
          saved: false,
          movie: {
            id: 346910,
            title: 'The Predator',
            posterPath: 'wMq9kQXTeQCHUZOG4fAe5cAxyUA.jpg',
            releaseDate: '2018-09-14',
          },
          comparandUser: {
            userId: '12',
            rating: 9,
            saved: false,
          },
        },
      ],
      pagingMetadata: { next: null, hasNext: false },
    }
    localStorageUtil.setTestIsLoggedIn(true)
    localStorageUtil.setTestServiceDataOverride([
      {
        service: 'loadCollectionAs',
        data: loadCollectionData,
      },
      {
        service: 'loadUserById',
        data: {
          id: 12,
          firstname: 'Sandra',
          lastname: 'Aguilar',
        },
      },
    ])

    cy.visit('/profile/12/collection')

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]')
      .should('have.length', 1)

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=not-me-user-text]')
      .contains('Sandra Aguilar Collection')

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]:first')
      .contains('User Rating: 9')

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]:first')
      .find('[data-testid=slider-movie-rating]')
      .contains('Your Rating 7')
  })
})
