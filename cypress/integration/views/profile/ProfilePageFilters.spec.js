/* eslint-disable */
/// <reference types="cypress" />
import { localStorageUtil } from '../../../../src/shared/util'

describe('Profile Page Filters', () => {
  beforeEach(() => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/profile/11/watchlist')
  })

  it('should render filters properly when on watchlist', () => {
    cy.get('[data-testid=profile-page]')
      .find('[data-testid=filters-button]')
      .click()

    cy.get('[data-testid=filters-container]').contains('Movie Genre')

    cy.get('[data-testid=filters-container]')
      .find('[data-testid=genre-chip]')
      .should('have.length', 19)

    cy.get('[data-testid=filters-container]').contains('Years Range')

    cy.get('[data-testid=filters-container]').contains('From 1900 to 2019')

    cy.get('[data-testid=filters-container]').find('.rc-slider')
  })

  it('should handle filters when on watchlist', () => {
    cy.get('[data-testid=profile-page]')
      .find('[data-testid=filters-button]')
      .click()

    cy.get('[data-testid=filters-container]').contains('Movie Genre')

    cy.get('[data-testid=filters-container]')
      .find('[data-testid=genre-chip]:first')
      .click()

    cy.get('[data-testid=filters-container]')
      .find('.rc-slider')
      .click()

    cy.url().should('include', '?genres=Action&yearsRange=')
  })

  it('should render properly when on watchlist and filters in the url', () => {
    cy.visit(
      '/profile/11/watchlist?genres=Action&yearsRange=1900&yearsRange=1961',
    )

    cy.get('[data-testid=profile-page]').find(
      '[data-testid=filter-cleaner-genres]',
    )

    cy.get('[data-testid=profile-page]').find(
      '[data-testid=filter-cleaner-years]',
    )

    cy.get('[data-testid=profile-page]').find(
      '[data-testid=filter-cleaner-all]',
    )
  })

  it('filter cleaners should work properly', () => {
    cy.visit(
      '/profile/11/watchlist?genres=Action&yearsRange=1900&yearsRange=1961',
    )

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=filter-cleaner-genres]')
      .click()

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=filter-cleaner-genres]')
      .should('have.length', 0)

    cy.url().should('include', '?yearsRange=1900&yearsRange=1961')

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=filter-cleaner-all]')
      .click()

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=filter-cleaner-all]')
      .should('have.length', 0)

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=filter-cleaner-years]')
      .should('have.length', 0)
  })

  it('should render filters properly when on collection', () => {
    cy.visit('/profile/11/collection')

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=filters-button]')
      .click()

    cy.get('[data-testid=filters-container]').contains('Movie Genre')

    cy.get('[data-testid=filters-container]')
      .find('[data-testid=genre-chip]')
      .should('have.length', 19)

    cy.get('[data-testid=filters-container]').contains('Minimum Rating')

    cy.get('[data-testid=filters-container]').contains('Years Range')

    cy.get('[data-testid=filters-container]').contains('From 1900 to 2019')

    cy.get('[data-testid=filters-container]').find('.rc-slider:first')
    cy.get('[data-testid=filters-container]').find('.rc-slider:last')
  })

  it('should handle filters when on collection', () => {
    cy.visit('/profile/11/collection')

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=filters-button]')
      .click()

    cy.get('[data-testid=filters-container]')
      .find('.rc-slider:first')
      .click()

    cy.url().should('include', '?minimumRating=5')
  })

  it('should hide minimumRating filter if moving from collection to watchlist', () => {
    cy.visit('/profile/11/collection?minimumRating=5')

    cy.get('[data-testid=profile-page]').find(
      '[data-testid=filter-cleaner-ratings]',
    )

    cy.get('[data-testid=header]')
      .find('[data-testid=user-menu-button]')
      .contains('Juan Morales')
      .click()

    cy.get('[data-testid=user-menu]')
      .find('[data-testid=watchlist-option]')
      .click()

    cy.get('[data-testid=profile-page]')
      .find('[data-testid=filter-cleaner-ratings]')
      .should('have.length', 0)
  })

  it('footer watchlist and collection buttons should work as expected having filters applied', () => {
    cy.visit(
      '/profile/11/watchlist?genres=Action&yearsRange=1900&yearsRange=1961',
    )

    cy.get('[data-testid=subfooter-collection-link]').click()

    cy.get('[data-testid=filter-cleaner-genres]')
    cy.get('[data-testid=filter-cleaner-years]')
    cy.get('[data-testid=filter-cleaner-all]')

    cy.get('[data-testid=filter-cleaner-years]').click()

    cy.get('[data-testid=subfooter-watchlist-link]').click()

    cy.get('[data-testid=filter-cleaner-genres]')
    cy.get('[data-testid=filter-cleaner-all]')
  })
})
