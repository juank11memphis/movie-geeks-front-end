/* eslint-disable */
/// <reference types="cypress" />
import { localStorageUtil } from '../../../../src/shared/util'

describe('Profile Page Filters Mobile', () => {
  beforeEach(() => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/profile/11/watchlist')
    cy.viewport('iphone-6')
  })

  it('should render apply button and close drawer when clicking on it', () => {
    cy.get('[data-testid=profile-page]')
      .find('[data-testid=filters-button]')
      .click()

    cy.get('[data-testid=filters-container]')
      .find('[data-testid=movie-filters-apply-button]')
      .click()
  })
})
