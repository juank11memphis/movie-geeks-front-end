/* eslint-disable */
/// <reference types="cypress" />
import { localStorageUtil } from '../../../../src/shared/util'

describe('Search Page', () => {
  beforeEach(() => {
    cy.visit('/search')
  })

  it('should handle errors when searching', () => {
    localStorageUtil.setTestResponseMode('error')

    cy.get('[data-testid=header]')
      .find('[data-testid=search-icon]')
      .click()

    cy.get('[data-testid=header]')
      .find('[data-testid=search-field] input')
      .type('fight club')

    cy.get('[data-testid=search-page]')
      .find('[data-testid=error-message-container]')
      .contains('An unexpected error has occurred')

    cy.get('[data-testid=search-page]')
      .find('[data-testid=error-message-container]')
      .contains('Mock Server Error')
  })

  it('should render properly when no data is returned', () => {
    localStorageUtil.setTestResponseMode('no-data')

    cy.get('[data-testid=search-page]')

    cy.get('[data-testid=header]')
      .find('[data-testid=search-icon]')
      .click()

    cy.get('[data-testid=header]')
      .find('[data-testid=search-field] input')
      .type('fight club')

    cy.get('[data-testid=search-page]').contains(
      'The search has no matches, try another keyword',
    )

    cy.get('[data-testid=search-page]').find('[data-testid=noResultsImage]')
  })

  it('should render search properly when there is data', () => {
    cy.get('[data-testid=search-page]')

    cy.get('[data-testid=header]')
      .find('[data-testid=search-icon]')
      .click()

    cy.get('[data-testid=header]')
      .find('[data-testid=search-field] input')
      .type('fight club')

    cy.get('[data-testid=search-page]').contains(
      'Showing search results for: "fight club"',
    )

    cy.get('[data-testid=search-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]')
      .should('have.length', 2)

    cy.get('[data-testid=search-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]:first')
      .contains('The Fight Club')

    cy.get('[data-testid=search-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=movie-card]:first')
      .find('[data-testid=add-to-watchlist-button]')
  })

  it('should hide the search input when navigating to a new page', () => {
    cy.get('[data-testid=search-page]')

    cy.get('[data-testid=header]')
      .find('[data-testid=search-icon]')
      .click()

    cy.get('[data-testid=header]')
      .find('[data-testid=search-field] input')
      .type('fight club')

    cy.get('[data-testid=header]')
      .find('[data-testid=brand-link]')
      .click()

    cy.get('[data-testid=header]')
      .find('[data-testid=brand-link]')
      .click()

    cy.get('[data-testid=header]')
      .find('[data-testid=search-field] input')
      .should('have.length', 0)
  })
})
