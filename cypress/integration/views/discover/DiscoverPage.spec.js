/* eslint-disable */
/// <reference types="cypress" />
import { localStorageUtil } from '../../../../src/shared/util'

describe('Discover Page', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('should not render the discover option when the user is not logged in', () => {
    cy.get('body')
      .find('[data-testid=header]')
      .find('[data-testid=discover-link]')
      .should('have.length', 0)
  })

  it('should render the discover option when the user is logged in', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')
    cy.get('body')
      .find('[data-testid=header]')
      .find('[data-testid=discover-link]')
      .should('have.length', 1)
  })

  it('should navigate to the discover page when clicking the discover link', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')
    cy.get('body')
      .find('[data-testid=header]')
      .find('[data-testid=discover-link]')
      .click()

    cy.url().should('include', 'discover')
  })

  it('should render the discover page when there is no data returned from the server', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    localStorageUtil.setTestResponseMode('no-data')
    cy.visit('/discover')

    cy.get('[data-testid=discover-page]').find(
      '[data-testid=discover-noresults-desktop-image]',
    )
  })

  it('should render the discover page when there is data', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/discover')

    cy.get('[data-testid=discover-page]').find(
      '[data-testid=discover-instructions]',
    )

    cy.get('[data-testid=discover-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=discover-movie-card]')
      .should('have.length', 4)

    cy.get('[data-testid=discover-page]')
      .find('[data-testid=movie-cards]')
      .find('[data-testid=discover-movie-card]:first')
      .find('[data-testid=movie-card-top-actions]')
      .find('[data-testid=trailer-button]')
  })

  it('should render properly when the server returns an error', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    localStorageUtil.setTestResponseMode('error')
    cy.visit('/discover')

    cy.get('[data-testid=discover-page]').find(
      '[data-testid=error-message-container]',
    )
  })
})
