/* eslint-disable */
/// <reference types="cypress" />
import { localStorageUtil } from '../../../../src/shared/util'

describe('header mobile', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.viewport('iphone-6')
  })

  it('should render properly', () => {
    cy.get('[data-testid=header]').find('[data-testid=brand-link]')

    cy.get('[data-testid=header]').find('[data-testid=open-drawer-button]')
  })

  it('should open the drawer and render options links', () => {
    cy.get('[data-testid=header]')
      .find('[data-testid=open-drawer-button]')
      .click()

    cy.get('[data-testid=drawer]')

    cy.get('[data-testid=drawer]')
      .find('[data-testid=signin-link]')
      .contains('Sign In')

    cy.get('[data-testid=drawer]')
      .find('[data-testid=signup-link]')
      .contains('Sign Up')
  })

  it('should navigate to home', () => {
    cy.get('[data-testid=header]')
      .find('[data-testid=open-drawer-button]')
      .click()

    cy.get('[data-testid=drawer]')
      .find('[data-testid=signin-link]')
      .click()

    cy.get('[data-testid=header]')
      .find('[data-testid=brand-link-mobile]')
      .click()

    cy.url().should('include', '/')
  })

  it('should navigate to signin page', () => {
    cy.get('[data-testid=header]')
      .find('[data-testid=open-drawer-button]')
      .click()

    cy.get('[data-testid=drawer]')
      .find('[data-testid=signin-link]')
      .click()

    cy.url().should('include', '/signin')
  })

  it('should navigate to signup', () => {
    cy.get('[data-testid=header]')
      .find('[data-testid=open-drawer-button]')
      .click()

    cy.get('[data-testid=drawer]')
      .find('[data-testid=signup-link]')
      .click()

    cy.url().should('include', '/signup')
  })

  it('should render properly when logged in', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=header]')
      .find('[data-testid=open-drawer-button]')
      .click()

    cy.get('[data-testid=drawer]')
      .find('[data-testid=signout-link]')
      .contains('Sign Out')
  })

  it('should redirect to watchlist page', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=header]')
      .find('[data-testid=open-drawer-button]')
      .click()

    cy.get('[data-testid=drawer]')
      .find('[data-testid=watchlist-link]')
      .click()

    cy.url().should('include', '/profile/11/watchlist')

    cy.get('[data-testid=profile-page]')
  })

  it('should redirect to collection page', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=header]')
      .find('[data-testid=open-drawer-button]')
      .click()

    cy.get('[data-testid=drawer]')
      .find('[data-testid=collection-link]')
      .click()

    cy.url().should('include', '/profile/11/collection')

    cy.get('[data-testid=profile-page]')
  })

  it('should handle sign out', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=header]')
      .find('[data-testid=open-drawer-button]')
      .click()

    cy.get('[data-testid=drawer]')
      .find('[data-testid=signout-link]')
      .contains('Sign Out')
      .click()

    cy.url().should('include', '/')

    cy.get('[data-testid=header]').find('[data-testid=brand-link]')
  })
})
