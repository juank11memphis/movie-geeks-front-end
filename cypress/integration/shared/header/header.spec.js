/* eslint-disable */
/// <reference types="cypress" />
import { localStorageUtil } from '../../../../src/shared/util'

describe('header', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('should render properly', () => {
    cy.get('[data-testid=header]').find('[data-testid=brand-link]')

    cy.get('[data-testid=header]')
      .find('[data-testid=signin-link]')
      .contains('Sign In')

    cy.get('[data-testid=header]')
      .find('[data-testid=signup-link]')
      .contains('Sign Up')

    cy.get('[data-testid=header]').find('[data-testid=search-icon]')
  })

  it('should navigate to home', () => {
    cy.get('[data-testid=header]')
      .find('[data-testid=signin-link]')
      .contains('Sign In')
      .click()

    cy.get('[data-testid=header]')
      .find('[data-testid=brand-link]')
      .click()

    cy.url().should('include', '/')
  })

  it('should navigate to signin page', () => {
    cy.get('[data-testid=header]')
      .find('[data-testid=signin-link]')
      .contains('Sign In')
      .click()

    cy.url().should('include', '/signin')
  })

  it('should navigate to signup', () => {
    cy.get('[data-testid=header]')
      .find('[data-testid=signup-link]')
      .contains('Sign Up')
      .click()

    cy.url().should('include', '/signup')
  })

  it('should render properly when logged in', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=header]').find('[data-testid=brand-link]')

    cy.get('[data-testid=header]')
      .find('[data-testid=user-menu-button]')
      .contains('Juan Morales')
  })

  it('should redirect to watchlist page', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=header]')
      .find('[data-testid=user-menu-button]')
      .contains('Juan Morales')
      .click()

    cy.get('[data-testid=user-menu]')
      .find('[data-testid=watchlist-option]')
      .click()

    cy.url().should('include', '/profile/11/watchlist')
    cy.get('[data-testid=profile-page]')
  })

  it('should redirect to collection page', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=header]')
      .find('[data-testid=user-menu-button]')
      .contains('Juan Morales')
      .click()

    cy.get('[data-testid=user-menu]')
      .find('[data-testid=collection-option]')
      .click()

    cy.url().should('include', '/profile/11/collection')
    cy.get('[data-testid=profile-page]')
  })

  it('should handle sign out', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=header]')
      .find('[data-testid=user-menu-button]')
      .contains('Juan Morales')
      .click()

    cy.get('[data-testid=user-menu]')
      .find('[data-testid=signout-option]')
      .click()

    cy.url().should('include', '/')

    cy.get('[data-testid=header]')
      .find('[data-testid=signin-link]')
      .contains('Sign In')
  })

  it('should handle transition to search state', () => {
    cy.get('[data-testid=header]')
      .find('[data-testid=search-icon]')
      .click()

    cy.get('[data-testid=header]').find('[data-testid=search-field]')

    cy.get('[data-testid=header]').find('[data-testid=clear-icon]')

    cy.get('[data-testid=header]')
      .find('[data-testid=clear-icon]')
      .click()

    cy.get('[data-testid=header]').find('[data-testid=search-icon]')
  })

  it('should navigate to search page after searching something', () => {
    cy.get('[data-testid=header]')
      .find('[data-testid=search-icon]')
      .click()

    cy.get('[data-testid=header]')
      .find('[data-testid=search-field] input')
      .type('fight club')

    cy.get('[data-testid=home-page]')

    cy.url().should('include', '/search')
    cy.location().should(loc => {
      expect(loc.search).to.eq('?query=fight%20club')
    })
  })
})
