/* eslint-disable */
/// <reference types="cypress" />

import { localStorageUtil } from '../../../../src/shared/util'

describe('Footer', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('should render properly', () => {
    const getCurrentYear = () => new Date().getFullYear()

    cy.get('[data-testid=footer]')

    cy.get('[data-testid=footer]')
      .find('h5')
      .contains(`${getCurrentYear()} © MovieGeeks`)
  })

  it('should render SubFooter options when logged out', () => {
    cy.get('[data-testid=subFooter]')
      .find('[data-testid=subfooter-home-link]')
      .contains('Home')

    cy.get('[data-testid=subFooter]')
      .find('[data-testid=subfooter-signin-link]')
      .contains('Sign In')

    cy.get('[data-testid=subFooter]')
      .find('[data-testid=subfooter-signup-link]')
      .contains('Sign Up')
  })

  it('home link should work properly', () => {
    cy.get('[data-testid=subFooter]')
      .find('[data-testid=subfooter-home-link]')
      .contains('Home')
      .click()

    cy.url().should('include', '/')

    cy.get('[data-testid=home-page]')
  })

  it('signup link should work properly', () => {
    cy.get('[data-testid=subFooter]')
      .find('[data-testid=subfooter-signup-link]')
      .contains('Sign Up')
      .click()

    cy.url().should('include', '/signup')

    cy.get('[data-testid=signup-page]')
  })

  it('signin link should work properly', () => {
    cy.get('[data-testid=subFooter]')
      .find('[data-testid=subfooter-signin-link]')
      .contains('Sign In')
      .click()

    cy.url().should('include', '/signin')

    cy.get('[data-testid=signin-page]')
  })

  it('should render SubFooter options when logged in', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=subFooter]')
      .find('[data-testid=subfooter-signedin-home-link]')
      .contains('Home')

    cy.get('[data-testid=subFooter]')
      .find('[data-testid=subfooter-collection-link]')
      .contains('Collection')

    cy.get('[data-testid=subFooter]')
      .find('[data-testid=subfooter-watchlist-link]')
      .contains('Watchlist')
  })

  it('home link should work properly when logged in', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=subFooter]')
      .find('[data-testid=subfooter-signedin-home-link]')
      .contains('Home')
      .click()

    cy.url().should('include', '/')

    cy.get('[data-testid=home-page]')
  })

  it('collection link should work properly', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=subFooter]')
      .find('[data-testid=subfooter-collection-link]')
      .contains('Collection')
      .click()

    cy.url().should('include', '/profile/11/collection')

    cy.get('[data-testid=profile-page]')
  })

  it('watchlist link should work properly', () => {
    localStorageUtil.setTestIsLoggedIn(true)
    cy.visit('/')

    cy.get('[data-testid=subFooter]')
      .find('[data-testid=subfooter-watchlist-link]')
      .contains('Watchlist')
      .click()

    cy.url().should('include', '/profile/11/watchlist')

    cy.get('[data-testid=profile-page]')
  })
})
