/* eslint-disable */

const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const optimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const uglifyJSPlugin = require('uglifyjs-webpack-plugin')
const compressionPlugin = require('compression-webpack-plugin')
const brotliPlugin = require('brotli-webpack-plugin')
const miniCssExtractPlugin = require('mini-css-extract-plugin')
const ManifestPlugin = require('webpack-manifest-plugin')
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin')

const common = require('./webpack.common')

module.exports = merge(common, {
  entry: {
    app: ['./src/index.js'],
  },
  output: {
    filename: '[name]-bundle-[hash:8].js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/',
  },
  mode: 'production',
  devtool: 'source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    new optimizeCssAssetsPlugin(),
    new miniCssExtractPlugin({
      filename: '[name]-[contenthash].css',
    }),
    new uglifyJSPlugin({
      exclude: 'vendors',
    }),
    new compressionPlugin({
      algorithm: 'gzip',
    }),
    new brotliPlugin(),
    new ManifestPlugin({ fileName: 'asset-manifest.json' }),
    new SWPrecacheWebpackPlugin({
      // By default, a cache-busting query parameter is appended to requests
      // used to populate the caches, to ensure the responses are fresh.
      // If a URL is already hashed by Webpack, then there is no concern
      // about it being stale, and the cache-busting can be skipped.
      dontCacheBustUrlsMatching: /\.\w{8}\./,
      filename: 'service-worker.js',
      logger(message) {
        if (message.indexOf('Total precache size is') === 0) {
          // This message occurs for every build and is a bit too noisy.
          return
        }
        console.log(message)
      },
      minify: true, // minify and uglify the script
      navigateFallback: '/index.html',
      staticFileGlobsIgnorePatterns: [/\.map$/, /asset-manifest\.json$/],
    }),
  ],
})
