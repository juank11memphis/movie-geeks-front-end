/* eslint-disable */

const express = require('express')
const path = require('path')
var proxy = require('http-proxy-middleware')

const server = express()

server.use(
  '/api',
  proxy({
    target: 'http://localhost:3001',
    changeOrigin: true,
  }),
)

server.use(
  '/graphql',
  proxy({
    target: 'http://localhost:3001',
    changeOrigin: true,
  }),
)

const expressStaticGzip = require('express-static-gzip')

server.use(
  expressStaticGzip('dist', {
    enableBrotli: true,
  }),
)

server.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname, 'dist/index.html'), function(err) {
    if (err) {
      res.status(500).send(err)
    }
  })
})

const PORT = 3000
server.listen(PORT, () => {
  console.log(`Server listening on http://localhost:${PORT}`)
})
