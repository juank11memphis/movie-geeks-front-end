# ---------------------------------
# RUNNING ON THE APP SERVER
# ---------------------------------

deploy-prod: init
	docker build -t prod-front-end .
	docker stop prod-front-end || true
	docker run --rm -d -e ENVIRONMENT=production --name prod-front-end --network="host" prod-front-end

# ----------------------------
# UTILITY FUNCTIONS
# ----------------------------

init:
	$(call init_yarn_cache)
	$(call create_network)

define create_network
	docker network create moviegeeks || true
endef

define init_yarn_cache
	# Init empty cache file if needed
	if [ ! -f .yarn-cache.tgz ]; then \
		echo "Init empty .yarn-cache.tgz"; \
		tar cvzf .yarn-cache.tgz --files-from /dev/null; \
	fi;
endef

# ----------------------------
# MAKE TARGETS
# ----------------------------

.PHONY: init
