import React from 'react'
import { Route, Switch } from 'react-router-dom'

import {
  HomePage,
  SignInPage,
  ForgotPasswordPage,
  SignUpPage,
  ResetPasswordPage,
  SearchPage,
  ProfilePage,
  DiscoverPage,
} from './views'

const AppRoutes = () => (
  <Switch>
    <Route exact path="/" render={() => <HomePage />} />
    <Route exact path="/signup" render={() => <SignUpPage />} />
    <Route exact path="/signin" render={() => <SignInPage />} />
    <Route
      exact
      path="/forgot-password"
      render={() => <ForgotPasswordPage />}
    />
    <Route exact path="/reset-password" render={() => <ResetPasswordPage />} />
    <Route exact path="/search" render={() => <SearchPage />} />
    <Route
      path="/profile/:id"
      render={({
        match: {
          params: { id },
        },
      }) => <ProfilePage userId={id} />}
    />
    <Route exact path="/discover" render={() => <DiscoverPage />} />
  </Switch>
)

export default AppRoutes
