import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import {
  Box,
  MovieCardImage,
  MovieCardHeader,
  MovieCardTopActions,
  SliderMovieRating,
} from '../../components'

import styles from './styles'

const DiscoverMovieCard = ({ classes, item }) => {
  const { movie } = item
  return (
    <Box
      align="center"
      style={{ display: 'flex' }}
      className={classes.movieCard}
      data-testid="discover-movie-card"
    >
      <MovieCardImage movie={movie} />
      <MovieCardTopActions movie={movie} />
      <div className={classes.movieCardInfo}>
        <MovieCardHeader movie={movie} />
        <SliderMovieRating movieId={movie.id} />
      </div>
    </Box>
  )
}

DiscoverMovieCard.propTypes = {
  classes: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(DiscoverMovieCard))
