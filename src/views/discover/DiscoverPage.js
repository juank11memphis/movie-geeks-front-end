import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import { useQuery } from 'react-apollo-hooks'
import { get } from 'lodash'

import { QueryResponseWrapper } from '../../components'
import { discoverQueries } from '../../shared/graphql'
import DiscoverResults from './DiscoverResults'

import styles from './styles'

const DiscoverPage = ({ classes }) => {
  const { data = {}, error, loading } = useQuery(discoverQueries.DISCOVER, {
    suspend: false,
    fetchPolicy: 'network-only',
  })
  const discoverItems = get(data, 'discover.items', [])
  return (
    <div data-testid="discover-page" className={classes.root}>
      <QueryResponseWrapper error={error} loading={loading}>
        <DiscoverResults items={discoverItems} />
      </QueryResponseWrapper>
    </div>
  )
}

DiscoverPage.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(DiscoverPage))
