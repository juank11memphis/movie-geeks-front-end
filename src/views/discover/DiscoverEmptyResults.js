import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import Hidden from '@material-ui/core/Hidden'

import { Image } from '../../components'

import styles from './styles'
import discoverNoResultsImage from '../../styles/img/discover-noresults.png'

const DiscoverEmptyResults = ({ classes }) => (
  <Fragment>
    <Hidden mdUp>
      <Image
        className={classes.noResultsImageMobile}
        image={discoverNoResultsImage}
        backgroundSize="contain"
        data-testid="discover-noresults-mobile-image"
      />
    </Hidden>
    <Hidden smDown>
      <Image
        className={classes.noResultsImage}
        image={discoverNoResultsImage}
        backgroundSize="contain"
        data-testid="discover-noresults-desktop-image"
      />
    </Hidden>
  </Fragment>
)

DiscoverEmptyResults.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(DiscoverEmptyResults))
