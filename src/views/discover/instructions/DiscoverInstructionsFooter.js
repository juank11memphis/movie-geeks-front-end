import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import { Box, Text } from '../../../components'
import styles from '../styles'

const DiscoverInstructionsFooter = ({ classes }) => (
  <Box
    clearPaddings
    className={classes.instructionsBottomContainer}
    direction="row"
    align="center"
  >
    <Text className={classes.bottomTextTitle} clearMargins bold color="white">
      REMEMBER
    </Text>
    <Text className={classes.bottomText} align="left" color="white">
      The more movies you rate, the more accurate results you get
    </Text>
  </Box>
)

DiscoverInstructionsFooter.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(DiscoverInstructionsFooter))
