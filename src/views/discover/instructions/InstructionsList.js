import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import { Box, Text } from '../../../components'

import styles from '../styles'

const InstructionsList = ({ classes }) => (
  <Box
    clearMargins
    clearPaddings
    className={classes.instructionsList}
    align="center"
  >
    <Box
      clearMargins
      clearPaddings
      direction="row"
      className={classes.instructionsItem}
    >
      <div className={classes.itemIndicator} />
      <Text align="left" color="white">
        We only show you well-rated movies by users with similar tastes to yours
      </Text>
    </Box>
    <Box
      clearMargins
      clearPaddings
      direction="row"
      className={classes.instructionsItem}
    >
      <div className={classes.itemIndicator} />
      <Text align="left" color="white">
        We do not show movies that you have already rated
      </Text>
    </Box>
    <Box
      clearMargins
      clearPaddings
      direction="row"
      className={classes.instructionsItem}
    >
      <div className={classes.itemIndicator} />
      <Text align="left" color="white">
        We do not show movies that you have already added to your watchlist
      </Text>
    </Box>
  </Box>
)

InstructionsList.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(InstructionsList))
