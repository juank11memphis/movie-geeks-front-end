import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import { Box, Text, Divider, Image } from '../../../components'
import DiscoverInstructionsFooter from './DiscoverInstructionsFooter'
import InstructionsList from './InstructionsList'

import styles from '../styles'
import discoverPirateImage from '../../../styles/img/discover-pirate.svg'

const DiscoverInstructions = ({ classes }) => (
  <Box clearMargins align="center" data-testid="discover-instructions">
    <Text align="center" clearMargins className={classes.title}>
      Find your next favorite movie!
    </Text>
    <Divider className={classes.divider} />
    <Box
      clearMargins
      clearPaddings
      className={classes.instructionsListContainer}
    >
      <Image className={classes.discoverImage} image={discoverPirateImage} />
      <InstructionsList />
      <DiscoverInstructionsFooter />
    </Box>
  </Box>
)

DiscoverInstructions.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(DiscoverInstructions))
