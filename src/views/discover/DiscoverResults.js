import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import { Box } from '../../components'
import DiscoverInstructions from './instructions/DiscoverInstructions'
import DiscoverEmptyResults from './DiscoverEmptyResults'
import DiscoverMovieCard from './DiscoverMovieCard'

import styles from './styles'

const DiscoverResults = ({ items }) => {
  if (!items || items.length === 0) {
    return <DiscoverEmptyResults />
  }
  return (
    <Fragment>
      <DiscoverInstructions />
      <Box direction="row" align="center" wrap="wrap" data-testid="movie-cards">
        {items.map(discoveryItem => (
          <DiscoverMovieCard key={discoveryItem.id} item={discoveryItem} />
        ))}
      </Box>
    </Fragment>
  )
}

DiscoverResults.propTypes = {
  classes: PropTypes.object.isRequired,
  items: PropTypes.array.isRequired,
}

export default withStyles(styles)(React.memo(DiscoverResults))
