import {
  defaultDivider,
  mediaQueryTablet,
  mediaQueryDesktop,
  defaultBoxShadow,
  blackBox,
} from '../../styles/main'
import colors from '../../styles/colors'

import movieCardStyles from '../../components/movies/styles'

export default {
  root: {
    paddingTop: 100,
    paddingBottom: 100,
    minHeight: 650,
  },
  divider: {
    ...defaultDivider,
    margin: '0 auto',
    marginBottom: 30,
  },
  movieCard: {
    ...movieCardStyles.movieCard,
    marginBottom: 10,
  },
  movieCardInfo: {
    ...movieCardStyles.movieCardInfo,
  },
  title: {
    fontWeight: 300,
    color: 'black',
    fontSize: 30,
  },
  instructionsListContainer: {
    ...blackBox,
    boxShadow: defaultBoxShadow,
    maxWidth: 1000,
    minWidth: 280,
    marginLeft: 50,
    paddingLeft: 60,
    paddingRight: 10,
    paddingBottom: 0,
    borderRadius: 2,
    marginBottom: 50,
    [mediaQueryDesktop]: {
      marginLeft: 0,
    },
    [mediaQueryTablet]: {
      marginLeft: 40,
    },
  },
  instructionsList: {
    [mediaQueryDesktop]: {
      flexDirection: 'row',
      alignItems: 'flex-start',
    },
    [mediaQueryTablet]: {
      flexDirection: 'row',
      alignItems: 'flex-start',
    },
  },
  instructionsItem: {
    marginBottom: 10,
    fontWeight: 400,
    maxWidth: 350,
  },
  instructionsBottomContainer: {
    boxSizing: 'border-box',
    backgroundColor: colors.mediumGrey2,
    height: 40,
    width: 'calc(100% + 70px)',
    marginLeft: -60,
    borderRadius: 2,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    paddingLeft: 5,
    paddingRight: 5,
    boxShadow: defaultBoxShadow,
    alignItems: 'center',
  },
  bottomTextTitle: {
    fontSize: '14px',
  },
  bottomText: {
    marginLeft: 8,
    lineHeight: '12px',
    fontSize: '14px',
    [mediaQueryDesktop]: {
      fontSize: '16px',
    },
    [mediaQueryTablet]: {
      fontSize: '16px',
    },
  },
  itemIndicator: {
    backgroundColor: colors.red,
    minWidth: 5,
    maxWidth: 5,
    height: 18,
    marginRight: 10,
    marginTop: 4,
  },
  discoverImage: {
    width: 100,
    height: 100,
    position: 'absolute',
    marginLeft: -110,
    marginTop: -50,
  },
  noResultsImageMobile: {
    margin: '0 auto',
    minWidth: 350,
    minHeight: 350,
  },
  noResultsImage: {
    margin: '0 auto',
    minWidth: 640,
    minHeight: 550,
  },
}
