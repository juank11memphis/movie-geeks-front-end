import { defaultRedButton } from '../../../styles/main'
import colors from '../../../styles/colors'

export default {
  buttons: {
    ...defaultRedButton,
  },
  filtersControls: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: colors.black,
    borderStyle: 'solid',
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 10,
  },
  filtersContainer: {
    width: 326,
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    overflowX: 'hidden',
  },
  openFiltersButton: {
    color: colors.red,
    padding: 0,
    fontSize: 20,
    fontWeight: 'normal',
    marginTop: 5,
    marginBottom: 5,
    minWidth: 100,
  },
  openFiltersIcon: {
    marginTop: -2,
  },
}
