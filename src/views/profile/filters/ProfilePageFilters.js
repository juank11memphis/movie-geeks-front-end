import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import queryString from 'query-string'
import { useStoreState, useStoreActions } from 'easy-peasy'
import Drawer from '@material-ui/core/Drawer'
import { withStyles } from '@material-ui/core'
import { withRouter } from 'react-router-dom'

import { useToggle } from '../../../shared/hooks'
import OpenFiltersButton from './OpenFiltersButton'
import { MovieFilters, Box, MovieFiltersCleaner } from '../../../components'

import styles from './styles'

const ProfilePageFilters = ({ classes, history, location }) => {
  const subRoute = useStoreState(state => state.profile.subRoute)
  const storeFilters = useStoreState(state => state.profile.filters)
  const setStoreFilters = useStoreActions(
    dispatch => dispatch.profile.setFilters,
  )
  const [isOpen, toggleDrawer] = useToggle(false, true)
  const closeDrawer = () => isOpen && toggleDrawer()

  const addFiltersToUrl = filters => {
    history.push({
      pathname: location.pathname,
      search: `?${queryString.stringify(filters)}`,
    })
  }
  const clearAllFilters = () => {
    setStoreFilters({})
    addFiltersToUrl({})
  }
  const clearFilter = filterProp => {
    const newFilters = { ...storeFilters, [filterProp]: undefined }
    setStoreFilters(newFilters)
    addFiltersToUrl(newFilters)
  }
  return (
    <Fragment>
      <Box
        data-testid="filters-container"
        className={classes.filtersControls}
        direction="row"
      >
        <OpenFiltersButton onClick={toggleDrawer} />
        <MovieFiltersCleaner
          filters={storeFilters}
          onClearAll={clearAllFilters}
          onClearGenres={() => clearFilter('genres')}
          onClearRatings={() => clearFilter('minimumRating')}
          onClearYears={() => clearFilter('yearsRange')}
          showMinimumRatingCleaner={subRoute === 'collection'}
        />
      </Box>
      <Drawer anchor="left" open={isOpen} onClose={() => closeDrawer()}>
        <div
          className={classes.filtersContainer}
          data-testid="filters-container"
        >
          <MovieFilters
            value={storeFilters}
            onChange={allFilters => {
              setStoreFilters(allFilters)
              addFiltersToUrl(allFilters)
            }}
            onApply={() => closeDrawer()}
            showMinimumRatingFilter={subRoute === 'collection'}
          />
        </div>
      </Drawer>
    </Fragment>
  )
}

ProfilePageFilters.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}

export default withStyles(styles)(withRouter(React.memo(ProfilePageFilters)))
