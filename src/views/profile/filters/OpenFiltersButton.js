import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core'
import FilterListIcon from '@material-ui/icons/FilterList'

import styles from './styles'

const OpenFiltersButton = ({ classes, onClick }) => (
  <Button
    className={classes.openFiltersButton}
    onClick={onClick}
    data-testid="filters-button"
  >
    Filters
    <FilterListIcon className={classes.openFiltersIcon} />
  </Button>
)

OpenFiltersButton.propTypes = {
  classes: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default withStyles(styles)(React.memo(OpenFiltersButton))
