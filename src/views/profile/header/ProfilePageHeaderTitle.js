import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import { get, startCase } from 'lodash'
import { useStoreState } from 'easy-peasy'

import { Text } from '../../../components'
import styles from '../styles'

const ProfilePageHeaderTitle = ({ classes, user }) => {
  const currentUser = useStoreState(state => state.currentUser.user)
  const subRoute = useStoreState(state => state.profile.subRoute)
  const currentUserId = get(currentUser, 'id')
  const userId = get(user, 'id')
  return (
    <Fragment>
      {userId === currentUserId && (
        <Text
          align="left"
          as="h2"
          color="black"
          clearMargins
          className={classes.title}
        >
          {startCase(subRoute)}
        </Text>
      )}
      {userId !== currentUserId && (
        <Text
          align="center"
          as="h2"
          color="black"
          clearMargins
          className={classes.title}
          data-testid="not-me-user-text"
        >
          {user.firstname} {user.lastname} {startCase(subRoute)}
        </Text>
      )}
    </Fragment>
  )
}

ProfilePageHeaderTitle.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(ProfilePageHeaderTitle))
