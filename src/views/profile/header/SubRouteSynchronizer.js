import React from 'react'
import PropTypes from 'prop-types'
import { useStoreState, useStoreActions } from 'easy-peasy'
import { withStyles } from '@material-ui/core'
import { withRouter } from 'react-router-dom'

const getActiveOptionFromUrl = location => {
  const routeUrl = location.pathname
  const activeOption = routeUrl.substring(
    routeUrl.lastIndexOf('/') + 1,
    routeUrl.length,
  )
  return activeOption
}

// Synchronizes currentSubRoute with sub route from url
const SubRouteSynchronizer = ({ location }) => {
  const subRoute = useStoreState(state => state.profile.subRoute)
  const setSubRoute = useStoreActions(dispatch => dispatch.profile.setSubRoute)
  const subRouteFromUrl = getActiveOptionFromUrl(location)
  if (subRoute !== subRouteFromUrl) {
    setSubRoute(subRouteFromUrl)
  }
  return null
}

SubRouteSynchronizer.propTypes = {
  location: PropTypes.object.isRequired,
}

export default withStyles({})(withRouter(React.memo(SubRouteSynchronizer)))
