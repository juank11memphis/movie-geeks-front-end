import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import { useStoreActions } from 'easy-peasy'
import { useQuery } from 'react-apollo-hooks'
import { withRouter } from 'react-router-dom'

import styles from '../styles'

import { routerUtil } from '../../../shared/util'
import { userQueries } from '../../../shared/graphql'
import { Divider, ConditionalRender } from '../../../components'
import ProfilePageFilters from '../filters/ProfilePageFilters'
import ProfilePageHeaderTitle from './ProfilePageHeaderTitle'
import SubRouteSynchronizer from './SubRouteSynchronizer'

const getFiltersFromUrl = location => {
  const filtersFromUrl = routerUtil.getQueryStringParamsFromLocation(location)
  if (filtersFromUrl.yearsRange) {
    filtersFromUrl.yearsRange = filtersFromUrl.yearsRange.map(year =>
      parseInt(year, 0),
    )
  }
  if (typeof filtersFromUrl.genres === 'string') {
    filtersFromUrl.genres = [filtersFromUrl.genres]
  }
  if (filtersFromUrl.minimumRating) {
    filtersFromUrl.minimumRating = parseInt(filtersFromUrl.minimumRating, 0)
  }
  return filtersFromUrl
}

const ProfilePageHeader = ({ classes, userId, location }) => {
  const { data } = useQuery(userQueries.LOAD_USER_BY_ID, {
    suspend: false,
    variables: { userId },
  })
  const setStoreFilters = useStoreActions(
    dispatch => dispatch.profile.setFilters,
  )
  const filtersFromUrl = getFiltersFromUrl(location)
  setStoreFilters(filtersFromUrl)
  return (
    <ConditionalRender
      condition={data.user !== undefined && data.user !== null}
    >
      <Fragment>
        <SubRouteSynchronizer />
        <ProfilePageHeaderTitle user={data.user} />
        <Divider className={classes.divider} />
        <ProfilePageFilters />
      </Fragment>
    </ConditionalRender>
  )
}

ProfilePageHeader.propTypes = {
  classes: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  userId: PropTypes.string.isRequired,
}

export default withStyles(styles)(withRouter(React.memo(ProfilePageHeader)))
