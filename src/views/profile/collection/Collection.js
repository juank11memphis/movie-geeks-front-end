import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import { useApolloClient } from 'react-apollo-hooks'
import { useStoreState, useStoreActions } from 'easy-peasy'
import { get } from 'lodash'
import InfiniteScroll from 'react-infinite-scroll-component'

import { userMovieQueries } from '../../../shared/graphql'
import {
  MovieCards,
  Text,
  QueryResponseWrapper,
  ConditionalRender,
  LoadingSpinner,
} from '../../../components'

import styles from '../styles'

const { LOAD_COLLECTION, LOAD_COLLECTION_AS } = userMovieQueries

const getQuery = (currentUserId, userId) => {
  if (currentUserId && currentUserId === userId) {
    return LOAD_COLLECTION
  }
  return LOAD_COLLECTION_AS
}

const getQueryParams = (currentUserId, userId, filters, next) => {
  const queryParams = {
    fetchPolicy: 'network-only',
    suspend: false,
  }
  const paging = { limit: 10, next }
  const queryVars = {
    mainUserId: currentUserId,
    params: { filters, paging },
  }
  if (currentUserId && currentUserId === userId) {
    return {
      ...queryParams,
      variables: { ...queryVars },
    }
  }
  return {
    ...queryParams,
    variables: {
      ...queryVars,
      impersonateUserId: userId,
    },
  }
}

const Collection = ({ classes, userId }) => {
  const profileFilters = useStoreState(state => state.profile.filters)
  const profileFiltersDirty = useStoreState(state => state.profile.filtersDirty)
  const setProfileFiltersDirty = useStoreActions(
    dispatch => dispatch.profile.setFiltersDirty,
  )
  const currentUser = useStoreState(state => state.currentUser.user)
  const currentUserId = get(currentUser, 'id')
  const [queryResponse, setQueryResponse] = useState({
    userMovies: [],
    alreadyFetch: false,
  })
  const apolloClient = useApolloClient()

  const fetchMoreData = async (currentUserMovies, next) => {
    const query = getQuery(currentUserId, userId)
    const queryParams = getQueryParams(
      currentUserId,
      userId,
      profileFilters,
      next,
    )
    try {
      const { data = {} } = await apolloClient.query({
        query,
        ...queryParams,
      })
      const newUserMovies = get(data, 'collection.userMovies', [])
      const next = get(data, 'collection.pagingMetadata.next')
      const hasNext = get(data, 'collection.pagingMetadata.hasNext', true)
      setQueryResponse({
        userMovies: [...currentUserMovies, ...newUserMovies],
        next,
        hasNext,
        alreadyFetch: true,
      })
    } catch (error) {
      setQueryResponse({
        userMovies: [],
        alreadyFetch: true,
        error,
      })
    }
  }
  if (!queryResponse.alreadyFetch || profileFiltersDirty) {
    setProfileFiltersDirty(false)
    fetchMoreData([], null)
  }
  return (
    <QueryResponseWrapper error={queryResponse.error}>
      <ConditionalRender
        condition={
          queryResponse.userMovies && queryResponse.userMovies.length > 0
        }
        fallback={<Text as="h5">No movies in collection</Text>}
      >
        <InfiniteScroll
          dataLength={queryResponse.userMovies.length}
          next={() =>
            fetchMoreData(queryResponse.userMovies, queryResponse.next)
          }
          hasMore={queryResponse.hasNext}
          hasChildren={queryResponse.userMovies.length > 0}
          loader={<LoadingSpinner />}
        >
          <MovieCards
            movieCardClassName={classes.movieCard}
            userMovies={queryResponse.userMovies}
          />
        </InfiniteScroll>
      </ConditionalRender>
    </QueryResponseWrapper>
  )
}

Collection.propTypes = {
  classes: PropTypes.object.isRequired,
  userId: PropTypes.string.isRequired,
}

export default withStyles(styles)(React.memo(Collection))
