import colors from '../../styles/colors'
import {
  mediaQueryMobile,
  mediaQueryTablet,
  defaultBackgroundImage,
  defaultDivider,
  defaultRedButton,
  defaultGrayButton,
} from '../../styles/main'

import backgroundImage from '../../styles/img/moviegeeks-user.jpg'

export default {
  root: {
    paddingTop: 150,
    paddingBottom: 100,
    marginBottom: 20,
    minHeight: 650,
    ...defaultBackgroundImage,
    backgroundImage: `url(${backgroundImage})`,
    backgroundAttachment: 'fixed',
  },
  contentContainer: {
    marginLeft: 250,
    marginRight: 250,
    [mediaQueryMobile]: {
      marginLeft: 20,
      marginRight: 20,
    },
    [mediaQueryTablet]: {
      marginLeft: 20,
      marginRight: 20,
    },
  },
  header: {
    marginBottom: 40,
  },
  movieCard: {
    marginBottom: 20,
  },
  selectedTab: {
    color: `${colors.red} !important`,
    borderBottomColor: `${colors.red} !important`,
  },
  title: {
    fontWeight: 300,
    color: 'black',
    fontSize: 30,
  },
  divider: {
    ...defaultDivider,
    margin: 0,
    marginTop: 10,
    marginBottom: 10,
  },
  buttonsContainer: {
    marginTop: 35,
    marginBottom: 35,
    justifyContent: 'space-evenly !important',
    [mediaQueryMobile]: {
      paddingLeft: 0,
      paddingRight: 0,
    },
  },
  button: {
    ...defaultGrayButton,
    width: 125,
    height: 30,
  },
  activeButton: {
    ...defaultRedButton,
    width: 125,
    height: 30,
  },
}
