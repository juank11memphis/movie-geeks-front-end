import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import { Route, Switch } from 'react-router-dom'
import { useStoreState } from 'easy-peasy'

import { Footer } from '../../components'
import ProfilePageHeader from './header/ProfilePageHeader'
import Watchlist from './watchlist/Watchlist'
import Collection from './collection/Collection'

import styles from './styles'

const ProfilePage = ({ classes, userId }) => {
  useStoreState(state => state.profile.subRoute)
  const storeFilters = useStoreState(state => state.profile.filters)
  const isCurrentUserFetched = useStoreState(state => state.currentUser.fetched)
  return (
    <Fragment>
      <div data-testid="profile-page">
        <div className={classes.root}>
          <div className={classes.contentContainer}>
            <ProfilePageHeader userId={userId} />
            <Switch>
              <Route
                path="/profile/:id/watchlist"
                render={() =>
                  isCurrentUserFetched &&
                  storeFilters && <Watchlist userId={userId} />
                }
              />
              <Route
                path="/profile/:id/collection"
                render={() =>
                  isCurrentUserFetched &&
                  storeFilters && <Collection userId={userId} />
                }
              />
            </Switch>
          </div>
        </div>
      </div>
      <Footer />
    </Fragment>
  )
}

ProfilePage.propTypes = {
  classes: PropTypes.object.isRequired,
  userId: PropTypes.string.isRequired,
}

export default withStyles(styles)(React.memo(ProfilePage))
