import React from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'

import { Footer, Banner } from '../../components'
import bannerImage from '../../styles/img/banner.jpg'
import PlayingNowMovies from './PlayingNowMovies'
import InfoPanel from './InfoPanel'

import styles from './styles'

const HomePage = ({ classes }) => (
  <div data-testid="home-page">
    <Banner
      image={bannerImage}
      mainText="Your minimalist online movie catalog"
      mainTextClassName={classes.bannerText}
      data-testid="home-banner"
    />
    <PlayingNowMovies data-testid="home-playing-now" />
    <InfoPanel data-testid="home-info-panel" />
    <Footer />
  </div>
)

HomePage.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(HomePage))
