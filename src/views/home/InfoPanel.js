import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import styles from './styles'
import logoSearch from '../../styles/img/logoSearch.svg'
import watchlistInfoImage from '../../styles/img/moviegeeks-home-watchlist.jpg'
import { Box, Text, Image, Divider } from '../../components'

const InfoPanel = ({ classes, ...rest }) => (
  <div {...rest}>
    <Box className={classes.reviewInfoPanel} direction="row" align="center">
      <Image
        image={logoSearch}
        width={200}
        height={200}
        backgroundSize="contain"
      />
      <Box className={classes.reviewInfoPanelText} align="left">
        <Text as="h3" align="left" className={classes.reviewInfoPanelText}>
          Track and review your favorite movies
        </Text>
        <Divider className={classes.divider} />
      </Box>
    </Box>
    <Box
      className={classes.watchlistInfoPanel}
      direction="row"
      align="center"
      style={{
        backgroundImage: `url(${watchlistInfoImage})`,
      }}
    >
      <Box align="right">
        <Text
          as="h3"
          align="right"
          color="white"
          className={classes.watchlistInfoPanelText}
        >
          Track everything you want to see by adding movies to your watchlist
        </Text>
        <Divider className={classes.divider} />
      </Box>
      <div className={classes.watchlistInfoSpacer} />
    </Box>
  </div>
)

InfoPanel.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(InfoPanel)
