import React from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import { useQuery } from 'react-apollo-hooks'

import styles from './styles'
import { movieQueries } from '../../shared/graphql'
import {
  Text,
  MoviesCarousel,
  Divider,
  QueryResponseWrapper,
} from '../../components'

const PlayingNowMovies = ({ classes, ...rest }) => {
  const { data, error, loading } = useQuery(movieQueries.GET_PLAYING_NOW, {
    suspend: false,
  })
  const { userMovies = [] } = data
  return (
    <QueryResponseWrapper error={error} loading={loading}>
      <div className={classes.blackBox}>
        <Text as="h3" align="center" color="white">
          Playing Now
        </Text>
        <Divider className={classes.centeredDivider} />
        <div className={classes.carouselContainer} {...rest}>
          <MoviesCarousel userMovies={userMovies} />
        </div>
      </div>
    </QueryResponseWrapper>
  )
}

PlayingNowMovies.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(PlayingNowMovies))
