import colors from '../../styles/colors'
import {
  mediaQueryMobile,
  blackBox,
  defaultDivider,
  defaultBackgroundImage,
} from '../../styles/main'

export default {
  bannerText: {
    width: 400,
    fontWeight: 100,
    [mediaQueryMobile]: {
      width: 200,
    },
  },
  blackBox: {
    ...blackBox,
  },
  carouselContainer: {
    height: 580,
    marginTop: 20,
    marginLeft: 40,
    marginRight: 40,
  },
  divider: {
    ...defaultDivider,
    margin: 0,
  },
  centeredDivider: {
    ...defaultDivider,
    margin: '0 auto',
  },
  reviewInfoPanel: {
    height: 500,
    [mediaQueryMobile]: {
      flexDirection: 'column',
    },
  },
  reviewInfoPanelText: {
    width: 300,
    fontWeight: '100 !important',
    [mediaQueryMobile]: {
      textAlign: 'center',
    },
  },
  watchlistInfoPanel: {
    ...defaultBackgroundImage,
    backgroundColor: colors.black,
    height: 500,
    backgroundPosition: 'right',
    [mediaQueryMobile]: {
      width: 'auto',
    },
    '@media (max-width: 1350px)': {
      backgroundPosition: '80%',
    },
    '@media (max-width: 1050px)': {
      backgroundPosition: '70%',
    },
  },
  watchlistInfoPanelText: {
    width: 400,
    fontWeight: '100 !important',
    [mediaQueryMobile]: {
      width: 230,
    },
  },
  watchlistInfoSpacer: {
    width: 200,
    height: 200,
    [mediaQueryMobile]: {
      width: 0,
      height: 0,
    },
  },
}
