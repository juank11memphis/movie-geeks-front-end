import HomePage from './home/HomePage'
import SignInPage from './auth/signin/SignInPage'
import SignUpPage from './auth/signup/SignUpPage'
import ForgotPasswordPage from './auth/forgot-password/ForgotPasswordPage'
import ResetPasswordPage from './auth/reset-password/ResetPasswordPage'
import SearchPage from './search/SearchPage'
import ProfilePage from './profile/ProfilePage'
import DiscoverPage from './discover/DiscoverPage'

export {
  HomePage,
  SignInPage,
  ForgotPasswordPage,
  SignUpPage,
  ResetPasswordPage,
  SearchPage,
  ProfilePage,
  DiscoverPage,
}
