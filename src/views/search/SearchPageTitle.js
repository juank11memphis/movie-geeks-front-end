import React from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'

import { Text, Divider } from '../../components'

import styles from './styles'

const SearchPageTitle = ({ classes, searchQuery, searchResults }) => {
  const getText = () => {
    if (searchQuery && searchResults.length > 0) {
      return (
        <Text as="h5" align="center" bold data-testid="search-header">
          Showing search results for: {`"${searchQuery}"`}
        </Text>
      )
    }
    if (searchQuery && searchResults.length <= 0) {
      return (
        <Text as="h5" align="center" bold data-testid="search-header">
          The search has no matches, try another keyword
        </Text>
      )
    }
    return (
      <Text as="h5" align="center" bold data-testid="search-header">
        No search terms entered
      </Text>
    )
  }
  return (
    <div>
      {getText()}
      <Divider className={classes.divider} />
    </div>
  )
}

SearchPageTitle.defaultProps = {
  searchQuery: null,
}

SearchPageTitle.propTypes = {
  classes: PropTypes.object.isRequired,
  searchResults: PropTypes.array.isRequired,
  searchQuery: PropTypes.string,
}

export default withStyles(styles)(React.memo(SearchPageTitle))
