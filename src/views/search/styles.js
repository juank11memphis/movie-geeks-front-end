import { defaultDivider } from '../../styles/main'

export default {
  root: {
    paddingTop: 200,
    paddingBottom: 100,
  },
  movieCard: {
    marginBottom: 10,
  },
  divider: {
    ...defaultDivider,
    margin: '0 auto',
    marginBottom: 30,
  },
  noResultsImage: {
    margin: '0 auto',
  },
}
