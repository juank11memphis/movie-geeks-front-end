import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import { useStoreState } from 'easy-peasy'
import { get } from 'lodash'
import { useQuery } from 'react-apollo-hooks'

import { Footer, QueryResponseWrapper } from '../../components'
import SearchPageTitle from './SearchPageTitle'
import SearchResults from './SearchResults'
import { movieQueries } from '../../shared/graphql'

import styles from './styles'

const SearchPage = ({ classes }) => {
  const searchQuery = useStoreState(state => state.search.query)
  const { data = {}, error, loading } = useQuery(movieQueries.SEARCH, {
    suspend: false,
    variables: { query: searchQuery || '' },
  })
  return (
    <Fragment>
      <div className={classes.root} data-testid="search-page">
        <QueryResponseWrapper error={error} loading={loading}>
          <Fragment>
            <SearchPageTitle
              searchQuery={searchQuery}
              searchResults={get(data.search, 'userMovies', [])}
            />
            <SearchResults
              searchQuery={searchQuery}
              searchResults={get(data.search, 'userMovies', [])}
            />
          </Fragment>
        </QueryResponseWrapper>
      </div>
      <Footer />
    </Fragment>
  )
}

SearchPage.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(SearchPage))
