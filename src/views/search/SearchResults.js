import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import styles from './styles'

import { MovieCards, Image } from '../../components'

import noResultsImage from '../../styles/img/logoSearchNotFound.svg'

const SearchResults = ({ classes, searchQuery, searchResults }) => {
  if (searchQuery && searchResults.length > 0) {
    return (
      <MovieCards
        movieCardClassName={classes.movieCard}
        userMovies={searchResults}
      />
    )
  }
  if (searchQuery && searchResults.length <= 0) {
    return (
      <Image
        className={classes.noResultsImage}
        image={noResultsImage}
        width={132}
        height={198}
        backgroundSize="contain"
        data-testid="noResultsImage"
      />
    )
  }
  return null
}

SearchResults.defaultProps = {
  searchQuery: null,
}

SearchResults.propTypes = {
  classes: PropTypes.object.isRequired,
  searchResults: PropTypes.array.isRequired,
  searchQuery: PropTypes.string,
}

export default withStyles(styles)(React.memo(SearchResults))
