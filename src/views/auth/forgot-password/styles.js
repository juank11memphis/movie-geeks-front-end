import {
  blackBox,
  defaultDivider,
  defaultRedButton,
  defaultBackgroundImage,
} from '../../../styles/main'

import backgroundImage from '../../../styles/img/signup-bg.jpg'

export default {
  root: {
    textAlign: 'center',
    paddingTop: 200,
    paddingBottom: 100,
    ...defaultBackgroundImage,
    backgroundImage: `url(${backgroundImage})`,
  },
  blackBox: {
    ...blackBox,
    width: 300,
    margin: '0 auto',
  },
  divider: {
    ...defaultDivider,
  },
  formContainer: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  buttons: {
    ...defaultRedButton,
    width: '100%',
  },
}
