import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core'
import { useMutation } from 'react-apollo-hooks'

import { authMutations } from '../../../shared/graphql'
import {
  FormContainer,
  FieldContainer,
  Input,
  Text,
  Divider,
} from '../../../components'
import { useToggle } from '../../../shared/hooks'
import validations from './ForgotPasswordFormValidations'

import styles from './styles'

const initialValues = {
  email: '',
}

const ForgotPasswordForm = ({ classes }) => {
  const [success, toggleSuccess] = useToggle(false, true)
  const requestPasswordReset = useMutation(authMutations.REQUEST_PASSWORD_RESET)
  const submitAction = ({ email }) =>
    requestPasswordReset({
      variables: { email },
    })
  return success ? (
    <div className={classes.blackBox}>
      <Text as="h3" align="center" color="white">
        Check your email for further instructions
      </Text>
    </div>
  ) : (
    <div className={classes.blackBox}>
      <Text as="h3" align="center" color="white">
        Enter your email
      </Text>
      <Divider className={classes.divider} />
      <FormContainer
        className={classes.formContainer}
        initialValues={initialValues}
        validations={validations}
        submitAction={submitAction}
        onSuccess={toggleSuccess}
        data-testid="forgot-password-form"
      >
        <FieldContainer
          name="email"
          comp={Input}
          placeholder="Your email address"
          data-testid="field-email"
        />
        <Button
          className={classes.buttons}
          color="primary"
          type="submit"
          data-testid="forgot-password-button"
        >
          Request Password Reset
        </Button>
      </FormContainer>
    </div>
  )
}

ForgotPasswordForm.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(ForgotPasswordForm))
