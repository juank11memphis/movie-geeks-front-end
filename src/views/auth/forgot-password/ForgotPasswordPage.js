import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import { Footer, AlreadyLoggedInRedirect } from '../../../components'
import ForgotPasswordForm from './ForgotPasswordForm'

import styles from './styles'

const ForgotPasswordPage = ({ classes }) => (
  <Fragment>
    <div className={classes.root} data-testid="forgot-password-page">
      <AlreadyLoggedInRedirect redirectTo="/" />
      <ForgotPasswordForm />
    </div>
    <Footer />
  </Fragment>
)

ForgotPasswordPage.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(ForgotPasswordPage))
