import { object, string } from 'yup'

const validations = object().shape({
  email: string()
    .email('Invalid email')
    .required('Your email is required'),
})

export default validations
