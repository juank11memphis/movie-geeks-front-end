import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import {
  Footer,
  AlreadyLoggedInRedirect,
  Text,
  Divider,
  LinkButton,
} from '../../../components'
import GoogleButton from '../GoogleButton'
import SignUpForm from './SignUpForm'
import styles from './styles'

const SignUpPage = ({ classes }) => (
  <Fragment>
    <div className={classes.root} data-testid="signup-page">
      <div className={classes.blackBox}>
        <Text as="h3" align="center" color="white">
          Sign Up
        </Text>
        <Divider className={classes.divider} />
        <GoogleButton
          className={classes.googleButton}
          text="Sign Up with Google"
          data-testid="google-button"
        />
      </div>
      <div className={classes.blackBox}>
        <Text as="h5" align="center" color="white">
          Or use your email
        </Text>
        <Divider className={classes.divider} />
        <SignUpForm />
      </div>
      <div className={classes.blackBox}>
        <Text
          as="h5"
          align="center"
          clearMargins
          data-testid="already-member"
          color="white"
        >
          Already a member?
        </Text>
        <Divider className={classes.divider} />
        <LinkButton
          className={classes.signInLink}
          text="Sign In"
          linkTo="signin"
          data-testid="signin-link"
        />
      </div>
    </div>
    <Footer />
    <AlreadyLoggedInRedirect redirectTo="/" />
  </Fragment>
)

SignUpPage.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(SignUpPage))
