import {
  blackBox,
  defaultDivider,
  defaultGoogleButton,
  defaultRedButton,
  defaultBackgroundImage,
} from '../../../styles/main'

import backgroundImage from '../../../styles/img/signup-bg.jpg'

export default {
  root: {
    textAlign: 'center',
    paddingTop: 200,
    paddingBottom: 100,
    ...defaultBackgroundImage,
    backgroundImage: `url(${backgroundImage})`,
  },
  blackBox: {
    ...blackBox,
    width: 300,
    margin: '0 auto',
    marginBottom: 20,
  },
  divider: {
    ...defaultDivider,
  },
  googleButton: {
    ...defaultGoogleButton,
  },
  inputs: {
    width: 325,
  },
  signInLink: {
    ...defaultRedButton,
  },
  formContainer: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  buttons: {
    ...defaultRedButton,
    width: '100%',
  },
}
