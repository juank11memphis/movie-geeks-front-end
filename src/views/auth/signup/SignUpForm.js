import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core'
import { useMutation } from 'react-apollo-hooks'
import { useStoreActions } from 'easy-peasy'

import { authMutations, movieQueries } from '../../../shared/graphql'
import {
  FormContainer,
  FieldContainer,
  Input,
  PasswordInput,
} from '../../../components'
import validations from './SignUpFormValidations'

import styles from './styles'

const initialValues = {
  firstname: '',
  lastname: '',
  email: '',
  password: '',
}

const SignUpForm = ({ classes }) => {
  const signUp = useMutation(authMutations.SIGN_UP, {
    refetchQueries: [{ query: movieQueries.GET_PLAYING_NOW }],
  })
  const setCurrentUser = useStoreActions(
    dispatch => dispatch.currentUser.setUser,
  )
  const submitAction = user =>
    signUp({
      variables: { user },
    })

  const onSuccess = ({ data }) => {
    const { auth } = data
    setCurrentUser(auth.user)
  }
  return (
    <FormContainer
      className={classes.formContainer}
      initialValues={initialValues}
      validations={validations}
      submitAction={submitAction}
      onSuccess={onSuccess}
      data-testid="signup-form"
    >
      <FieldContainer
        name="firstname"
        comp={Input}
        placeholder="Your first name"
        data-testid="field-firstname"
      />
      <FieldContainer
        name="lastname"
        comp={Input}
        placeholder="Your last name"
        data-testid="field-lastname"
      />
      <FieldContainer
        name="email"
        comp={Input}
        placeholder="Your email address"
        data-testid="field-email"
      />
      <FieldContainer
        name="password"
        comp={PasswordInput}
        placeholder="Your password"
        data-testid="field-password"
      />
      <Button
        color="primary"
        type="submit"
        data-testid="signup-button"
        className={classes.buttons}
      >
        Sign Up
      </Button>
    </FormContainer>
  )
}

SignUpForm.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(SignUpForm))
