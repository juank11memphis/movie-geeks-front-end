import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import { GoogleLogin } from 'react-google-login'
import classnames from 'classnames'
import { useStoreActions } from 'easy-peasy'
import { useMutation } from 'react-apollo-hooks'

import styles from './styles'
import { Box, Text } from '../../components'
import { authMutations, movieQueries } from '../../shared/graphql'

const onGoogleResponse = (response, onLogin) => {
  if (response.error) {
    return
  }
  const { profileObj } = response
  onLogin({
    firstname: profileObj.givenName,
    lastname: profileObj.familyName,
    email: profileObj.email,
  })
}

const GoogleButton = ({ text, classes, className, ...rest }) => {
  const setCurrentUser = useStoreActions(
    dispatch => dispatch.currentUser.setUser,
  )
  const authSocial = useMutation(authMutations.AUTH_SOCIAL, {
    refetchQueries: [{ query: movieQueries.GET_PLAYING_NOW }],
  })
  const onLogin = async user => {
    const { data } = await authSocial({ variables: { user } })
    const { auth } = data
    setCurrentUser(auth.user)
  }
  return (
    <GoogleLogin
      className={classnames(classes.googleButton, className)}
      clientId="82326996722-d8rr7ojaf4mfl5j92t5ggcpsf2d39oer.apps.googleusercontent.com"
      onSuccess={response => onGoogleResponse(response, onLogin)}
      onFailure={response => onGoogleResponse(response, onLogin)}
      uxMode="popup"
      {...rest}
    >
      <Box
        direction="row"
        align="center"
        className={classes.googleButtonContainer}
      >
        <Text color="white" className={classes.googleButtonText} bold>
          {text}
        </Text>
      </Box>
    </GoogleLogin>
  )
}

GoogleButton.defaultProps = {
  className: null,
}

GoogleButton.propTypes = {
  classes: PropTypes.object.isRequired,
  text: PropTypes.string.isRequired,
  className: PropTypes.string,
}

export default withStyles(styles)(React.memo(GoogleButton))
