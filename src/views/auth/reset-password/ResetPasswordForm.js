import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core'
import { useStoreActions } from 'easy-peasy'
import { useMutation } from 'react-apollo-hooks'

import { authMutations, movieQueries } from '../../../shared/graphql'
import { routerUtil, localStorageUtil } from '../../../shared/util'
import {
  FormContainer,
  FieldContainer,
  PasswordInput,
  Text,
  Divider,
} from '../../../components'
import validations from './ResetPasswordFormValidations'

import styles from './styles'

const initialValues = {
  password: '',
}

const ResetPasswordForm = ({ classes }) => {
  const resetToken = routerUtil.getQueryStringParam('token')
  localStorageUtil.storeResetPasswordToken(resetToken)
  const setCurrentUser = useStoreActions(
    dispatch => dispatch.currentUser.setUser,
  )
  const resetPasswordReset = useMutation(authMutations.RESET_PASSWORD, {
    refetchQueries: [{ query: movieQueries.GET_PLAYING_NOW }],
  })
  const submitAction = ({ password }) =>
    resetPasswordReset({
      variables: { password },
    })
  const onSuccess = ({ data }) => {
    const { auth } = data
    setCurrentUser(auth.user)
  }
  return (
    <div className={classes.blackBox}>
      <Text as="h3" align="center" color="white">
        Enter your new password
      </Text>
      <Divider className={classes.divider} />
      <FormContainer
        className={classes.formContainer}
        initialValues={initialValues}
        validations={validations}
        submitAction={submitAction}
        onSuccess={onSuccess}
        data-testid="reset-password-form"
      >
        <FieldContainer
          name="password"
          comp={PasswordInput}
          placeholder="Your new password"
          data-testid="field-password"
        />
        <Button
          className={classes.buttons}
          color="primary"
          type="submit"
          data-testid="reset-password-button"
        >
          Reset Password
        </Button>
      </FormContainer>
    </div>
  )
}

ResetPasswordForm.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(ResetPasswordForm))
