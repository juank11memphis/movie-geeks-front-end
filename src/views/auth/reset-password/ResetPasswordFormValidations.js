import { object, string } from 'yup'

const validations = object().shape({
  password: string()
    .min(6, 'Password must have at least 6 characters')
    .required('Your password is required'),
})

export default validations
