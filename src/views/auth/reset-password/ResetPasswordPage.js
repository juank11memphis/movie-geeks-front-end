import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import { Footer, AlreadyLoggedInRedirect } from '../../../components'
import ResetPasswordForm from './ResetPasswordForm'

import styles from './styles'

const ResetPasswordPage = ({ classes }) => (
  <Fragment>
    <div className={classes.root} data-testid="reset-password-page">
      <AlreadyLoggedInRedirect redirectTo="/" />
      <ResetPasswordForm />
    </div>
    <Footer />
  </Fragment>
)

ResetPasswordPage.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(ResetPasswordPage))
