import {
  blackBox,
  defaultDivider,
  defaultRedButton,
} from '../../../styles/main'

export default {
  root: {
    textAlign: 'center',
    paddingTop: 200,
    paddingBottom: 100,
  },
  formContainer: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  blackBox: {
    ...blackBox,
    width: 300,
    margin: '0 auto',
  },
  divider: {
    ...defaultDivider,
  },
  buttons: {
    ...defaultRedButton,
    width: '100%',
  },
}
