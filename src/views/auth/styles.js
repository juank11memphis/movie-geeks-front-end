export default {
  googleButtonContainer: {
    padding: 0,
  },
  googleButton: {},
  googleIcon: {
    marginRight: 10,
  },
  googleButtonText: {
    marginTop: 1,
  },
}
