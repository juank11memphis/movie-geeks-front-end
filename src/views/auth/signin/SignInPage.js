import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import {
  Footer,
  Text,
  Divider,
  AlreadyLoggedInRedirect,
  LinkButton,
} from '../../../components'
import styles from './styles'
import GoogleButton from '../GoogleButton'
import SignInForm from './SignInForm'

const SignInPage = ({ classes }) => (
  <Fragment>
    <div className={classes.root} data-testid="signin-page">
      <div className={classes.blackBox}>
        <Text as="h3" align="center" color="white">
          Sign In
        </Text>
        <Divider className={classes.divider} />
        <GoogleButton
          className={classes.googleButton}
          text="Sign In with Google"
          data-testid="google-button"
        />
      </div>
      <div className={classes.blackBox}>
        <Text as="h5" align="center" color="white">
          Or use your email
        </Text>
        <Divider className={classes.divider} />
        <SignInForm />
        <LinkButton
          className={classes.forgotPasswordLink}
          textClassName={classes.forgotPasswordLink}
          text="I have forgotten my password"
          linkTo="forgot-password"
          data-testid="forgot-password-link"
        />
      </div>
      <div className={classes.blackBox}>
        <Text
          as="h5"
          align="center"
          clearMargins
          color="white"
          data-testid="new-to-moviegeeks"
        >
          New to MovieGeeks?
        </Text>
        <Divider className={classes.divider} />
        <LinkButton
          className={classes.signupButton}
          text="Sign Up"
          linkTo="signup"
          data-testid="signup-link"
        />
      </div>
    </div>
    <Footer />
    <AlreadyLoggedInRedirect redirectTo="/" />
  </Fragment>
)

SignInPage.propTypes = {
  classes: PropTypes.object.isRequired,
}

const SignInPagePure = React.memo(SignInPage)
const SignInPageStyled = withStyles(styles)(SignInPagePure)

export default SignInPageStyled
