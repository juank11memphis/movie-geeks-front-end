import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core'
import { useMutation } from 'react-apollo-hooks'
import { useStoreActions } from 'easy-peasy'

import { authMutations, movieQueries } from '../../../shared/graphql'
import {
  FormContainer,
  FieldContainer,
  Input,
  PasswordInput,
} from '../../../components'
import validations from './SignInFormValidations'

import styles from './styles'

const initialValues = {
  email: '',
  password: '',
}

const SignInForm = ({ classes }) => {
  const setCurrentUser = useStoreActions(
    dispatch => dispatch.currentUser.setUser,
  )
  const signIn = useMutation(authMutations.SIGN_IN, {
    refetchQueries: [{ query: movieQueries.GET_PLAYING_NOW }],
  })
  const submitAction = ({ email, password }) =>
    signIn({
      variables: { email, password },
    })

  const onSuccess = ({ data }) => {
    const { auth } = data
    setCurrentUser(auth.user)
  }
  return (
    <FormContainer
      className={classes.formContainer}
      initialValues={initialValues}
      validations={validations}
      submitAction={submitAction}
      onSuccess={onSuccess}
      data-testid="signin-form"
    >
      <FieldContainer
        name="email"
        comp={Input}
        placeholder="Your email address"
        data-testid="field-email"
      />
      <FieldContainer
        name="password"
        comp={PasswordInput}
        placeholder="Your password"
        data-testid="field-password"
      />
      <Button
        className={classes.buttons}
        type="submit"
        data-testid="signin-button"
      >
        Sign In
      </Button>
    </FormContainer>
  )
}

SignInForm.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(SignInForm))
