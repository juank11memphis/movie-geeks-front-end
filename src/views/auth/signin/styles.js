import colors from '../../../styles/colors'
import {
  defaultBackgroundImage,
  blackBox,
  defaultRedButton,
  defaultGoogleButton,
  defaultDivider,
} from '../../../styles/main'

import backgroundImage from '../../../styles/img/signin-bg.jpg'

export default {
  root: {
    textAlign: 'center',
    paddingTop: 200,
    paddingBottom: 100,
    ...defaultBackgroundImage,
    backgroundImage: `url(${backgroundImage})`,
  },
  formContainer: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  divider: {
    ...defaultDivider,
  },
  forgotPasswordLink: {
    color: colors.white,
    fontSize: 16,
    padding: 0,
  },
  signUpLink: {
    color: colors.blue,
    fontSize: 16,
  },
  blackBox: {
    ...blackBox,
    width: 300,
    margin: '0 auto',
    marginBottom: 20,
  },
  buttons: {
    ...defaultRedButton,
    width: '100%',
  },
  signupButton: {
    ...defaultRedButton,
  },
  googleButton: {
    ...defaultGoogleButton,
  },
}
