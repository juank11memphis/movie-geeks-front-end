import grey from '@material-ui/core/colors/grey'
import blue from '@material-ui/core/colors/blue'

export default {
  white: grey['50'],
  lightGrey: grey['600'],
  mediumGrey: '#bfbfbf',
  mediumGrey2: '#666666',
  darkGrey: grey['800'],
  black: grey['900'],
  red: '#C32026',
  blue: blue['600'],
}
