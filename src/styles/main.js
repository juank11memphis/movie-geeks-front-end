import colors from './colors'

const mediaQueryMobileSuperSmall = '@media (max-width: 400px)'

const mediaQueryMobile = '@media (max-width: 576px)'

const mediaQueryTablet = '@media (min-width: 577px) and (max-width: 1199px)'

const mediaQueryDesktop = '@media (min-width: 1200px)'

const containerFluid = {
  paddingRight: '15px',
  paddingLeft: '15px',
  marginRight: 'auto',
  marginLeft: 'auto',
  width: '100%',
}

const container = {
  ...containerFluid,
  '@media (min-width: 576px)': {
    maxWidth: '540px',
  },
  '@media (min-width: 768px)': {
    maxWidth: '720px',
  },
  '@media (min-width: 992px)': {
    maxWidth: '960px',
  },
  '@media (min-width: 1200px)': {
    maxWidth: '1140px',
  },
}

const defaultFont = {
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  fontWeight: '300',
  lineHeight: '1.5em',
}

const title = {
  color: '#3C4858',
  textDecoration: 'none',
  lineHeight: '1.15em',
}

const h1 = {
  ...title,
  margin: '1.75rem 0 0.875rem',
  fontWeight: '600',
  fontSize: '4.2rem',
  [mediaQueryMobile]: {
    fontSize: '3.2rem',
  },
}

const h2 = {
  ...title,
  fontWeight: '300',
  margin: '10px 0 10px',
  fontSize: '3.313rem',
  [mediaQueryMobile]: {
    fontSize: '1.8rem',
  },
}

const h3 = {
  ...title,
  fontWeight: '100',
  margin: '10px 0 10px',
  fontSize: '2.313rem',
  [mediaQueryMobile]: {
    fontSize: '1.5rem',
  },
}

const h5 = {
  ...title,
  fontWeight: '100',
  margin: '10px 0 30px',
  fontSize: '1.2rem',
  [mediaQueryMobile]: {
    fontSize: '1rem',
  },
}

const defaultButton = {
  ...defaultFont,
  lineHeight: '30px',
  fontSize: '20px',
  borderRadius: '3px',
  textTransform: 'none',
  color: 'inherit',
  '&:hover,&:focus': {
    color: 'inherit',
    background: 'transparent',
  },
}

const defaultRedButton = {
  ...defaultFont,
  borderRadius: 4,
  backgroundColor: [colors.red, '!important'],
  '&:hover': {
    backgroundColor: [colors.red, '!important'],
  },
  minHeight: 30,
  maxHeight: 30,
  padding: 0,
  paddingLeft: 10,
  paddingRight: 10,
  textTransform: 'none',
  fontWeight: '600 !important',
  color: [colors.white, '!important'],
  '&:hover,&:focus': {
    color: 'inherit',
    background: 'transparent',
  },
}

const defaultGrayButton = {
  ...defaultRedButton,
  backgroundColor: [colors.darkGrey, '!important'],
  '&:hover': {
    backgroundColor: [colors.darkGrey, '!important'],
  },
}

const defaultGoogleButton = {
  ...defaultRedButton,
  minHeight: 50,
  maxHeight: 50,
  paddingLeft: '5px !important',
  paddingRight: '5px !important',
}

const noMargin = {
  margin: 0,
}

const defaultBoxShadow =
  '0 16px 24px 2px rgba(0,0,0,.14), 0 6px 30px 5px rgba(0,0,0,.12), 0 8px 10px -5px rgba(0,0,0,.2)'

const defaultBackgroundImage = {
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center',
}

const blackBox = {
  backgroundColor: 'rgba(0, 0, 0, 0.7)',
  boxShadow: '0px 0px 4px 0px rgba(0,0,0,0.7)',
  paddingTop: 20,
  paddingBottom: 20,
  boxSizing: 'border-box',
  borderRadius: 4,
}

const defaultDivider = {
  marginTop: 15,
  marginBottom: 15,
  width: 100,
  backgroundColor: colors.red,
  margin: '0 auto',
  height: 2,
}

export {
  container,
  defaultFont,
  title,
  h1,
  h2,
  h3,
  h5,
  defaultButton,
  defaultRedButton,
  defaultGrayButton,
  mediaQueryMobileSuperSmall,
  mediaQueryMobile,
  mediaQueryTablet,
  mediaQueryDesktop,
  defaultBoxShadow,
  noMargin,
  defaultBackgroundImage,
  blackBox,
  defaultGoogleButton,
  defaultDivider,
}
