import * as movieQueries from './queries/movieQueries'
import * as authQueries from './queries/authQueries'
import * as userMovieQueries from './queries/userMovieQueries'
import * as userQueries from './queries/userQueries'
import * as discoverQueries from './queries/discoverQueries'
import * as authMutations from './mutations/authMutations'
import * as userMovieMutations from './mutations/userMovieMutations'

export {
  movieQueries,
  authQueries,
  userMovieQueries,
  userQueries,
  discoverQueries,
  userMovieMutations,
  authMutations,
}
