import gql from 'graphql-tag'

import { movieInfo } from './movieFragments'

export const discoverItemFull = gql`
  fragment DiscoverItemFull on DiscoverItem {
    id
    movieId
    averageRating
    totalRating
    count
    movie {
      ...MovieInfo
    }
  }
  ${movieInfo}
`

export const discoverFull = gql`
  fragment DiscoverFull on Discover {
    items {
      ...DiscoverItemFull
    }
  }
  ${discoverItemFull}
`
