import gql from 'graphql-tag'

export const movieInfo = gql`
  fragment MovieInfo on Movie {
    id
    title
    originalTitle
    posterPath
    genres
    overview
    releaseDate
    year
    youtubeTrailerKey
  }
`
