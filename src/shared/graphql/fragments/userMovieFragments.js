import gql from 'graphql-tag'

import * as movieFragments from './movieFragments'

export const userMovieShort = gql`
  fragment UserMovieShort on UserMovie {
    id
    userId
    movieId
    rating
    saved
  }
`

export const userMovieFull = gql`
  fragment UserMovieFull on UserMovie {
    id
    userId
    movieId
    rating
    saved
    movie {
      ...MovieInfo
    }
    comparandUser {
      userId
      rating
      saved
    }
  }
  ${movieFragments.movieInfo}
`
export const userMoviesResponseFull = gql`
  fragment UserMoviesResponseFull on UserMoviesResponse {
    userMovies {
      ...UserMovieFull
    }
    pagingMetadata {
      hasNext
      next
    }
  }
  ${userMovieFull}
`
