import gql from 'graphql-tag'

import { userInfoFull } from '../fragments/userFragments'

export const SIGN_IN = gql`
  mutation signin($email: String, $password: String) {
    auth: signin(email: $email, password: $password) {
      token
      user {
        ...UserInfoFull
      }
    }
  }
  ${userInfoFull}
`

export const SIGN_UP = gql`
  mutation signup($user: UserInput) {
    auth: signup(user: $user) {
      token
      user {
        ...UserInfoFull
      }
    }
  }
  ${userInfoFull}
`

export const AUTH_SOCIAL = gql`
  mutation authSocial($user: UserInput) {
    auth: authSocial(user: $user) {
      token
      user {
        ...UserInfoFull
      }
    }
  }
  ${userInfoFull}
`

export const REQUEST_PASSWORD_RESET = gql`
  mutation requestPasswordRest($email: String) {
    requestPasswordReset(email: $email) {
      id
    }
  }
`

export const RESET_PASSWORD = gql`
  mutation resetPassword($password: String) {
    auth: resetPassword(password: $password) {
      token
      user {
        ...UserInfoFull
      }
    }
  }
  ${userInfoFull}
`
