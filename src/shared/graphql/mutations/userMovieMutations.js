import gql from 'graphql-tag'

import { userMovieShort } from '../fragments/userMovieFragments'

export const ADD_TO_WATCHLIST = gql`
  mutation addToWatchlist($movieId: String) {
    userMovie: addToWatchlist(movieId: $movieId) {
      ...UserMovieShort
    }
  }
  ${userMovieShort}
`

export const REMOVE_FROM_WATCHLIST = gql`
  mutation removeFromWatchlist($movieId: String) {
    userMovie: removeFromWatchlist(movieId: $movieId) {
      ...UserMovieShort
    }
  }
  ${userMovieShort}
`

export const RATE_MOVIE = gql`
  mutation rateMovie($movieId: String, $rating: Float) {
    userMovie: rateMovie(movieId: $movieId, rating: $rating) {
      ...UserMovieShort
    }
  }
  ${userMovieShort}
`

export const REMOVE_MOVIE_RATING = gql`
  mutation removeMovieRating($movieId: String) {
    userMovie: removeMovieRating(movieId: $movieId) {
      ...UserMovieShort
    }
  }
  ${userMovieShort}
`
