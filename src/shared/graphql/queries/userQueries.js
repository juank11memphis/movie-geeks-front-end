import gql from 'graphql-tag'

import { userInfoShort } from '../fragments/userFragments'

export const LOAD_USER_BY_ID = gql`
  query loadUserById($userId: String) {
    user: loadUserById(userId: $userId) {
      ...UserInfoShort
    }
  }
  ${userInfoShort}
`
