import gql from 'graphql-tag'

import { discoverFull } from '../fragments/discoverFragments'

export const DISCOVER = gql`
  query discover {
    discover {
      ...DiscoverFull
    }
  }
  ${discoverFull}
`
