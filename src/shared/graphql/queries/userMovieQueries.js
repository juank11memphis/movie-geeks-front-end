import gql from 'graphql-tag'

import * as userMovieFragments from '../fragments/userMovieFragments'

export const LOAD_WATCHLIST = gql`
  query loadWatchlist(
    $mainUserId: String
    $comparandUserId: String
    $params: MovieParams
  ) {
    watchlist: loadWatchlist(
      mainUserId: $mainUserId
      comparandUserId: $comparandUserId
      params: $params
    ) {
      ...UserMoviesResponseFull
    }
  }
  ${userMovieFragments.userMoviesResponseFull}
`

export const LOAD_WATCHLIST_AS = gql`
  query loadWatchlistAs(
    $mainUserId: String
    $impersonateUserId: String
    $params: MovieParams
  ) {
    watchlist: loadWatchlistAs(
      mainUserId: $mainUserId
      impersonateUserId: $impersonateUserId
      params: $params
    ) {
      ...UserMoviesResponseFull
    }
  }
  ${userMovieFragments.userMoviesResponseFull}
`

export const LOAD_COLLECTION = gql`
  query loadCollection(
    $mainUserId: String
    $comparandUserId: String
    $params: MovieParams
  ) {
    collection: loadCollection(
      mainUserId: $mainUserId
      comparandUserId: $comparandUserId
      params: $params
    ) {
      ...UserMoviesResponseFull
    }
  }
  ${userMovieFragments.userMoviesResponseFull}
`

export const LOAD_COLLECTION_AS = gql`
  query loadCollectionAs(
    $mainUserId: String
    $impersonateUserId: String
    $params: MovieParams
  ) {
    collection: loadCollectionAs(
      mainUserId: $mainUserId
      impersonateUserId: $impersonateUserId
      params: $params
    ) {
      ...UserMoviesResponseFull
    }
  }
  ${userMovieFragments.userMoviesResponseFull}
`
