import gql from 'graphql-tag'

import * as userMovieFragments from '../fragments/userMovieFragments'

export const GET_PLAYING_NOW = gql`
  query getPlayingNow {
    userMovies: getPlayingNow {
      ...UserMovieFull
    }
  }
  ${userMovieFragments.userMovieFull}
`

export const SEARCH = gql`
  query search($query: String) {
    search(query: $query) {
      page
      totalPages
      totalResults
      userMovies {
        ...UserMovieFull
      }
    }
  }
  ${userMovieFragments.userMovieFull}
`

export const GET_YOUTUBE_OFFICIAL_TRAILER_KEY = gql`
  query getYoutubeOfficialTrailerKey($movieId: String) {
    key: getYoutubeOfficialTrailerKey(movieId: $movieId)
  }
`
