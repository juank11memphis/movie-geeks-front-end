import gql from 'graphql-tag'

import { userInfoFull } from '../fragments/userFragments'

export const REFRESH_TOKEN = gql`
  mutation refreshToken {
    auth: refreshToken {
      token
      user {
        ...UserInfoFull
      }
    }
  }
  ${userInfoFull}
`
