import queryString from 'query-string'

export const getCurrentPath = location => {
  const fullPathName = location.pathname
  return fullPathName.substring(
    fullPathName.lastIndexOf('/') + 1,
    fullPathName.length,
  )
}

export const getQueryStringParams = () =>
  new URLSearchParams(window.location.search)

export const getQueryStringParam = param => {
  const params = getQueryStringParams()
  return params.get(param)
}

export const getQueryStringParamsFromLocation = location => {
  return queryString.parse(location.search)
}
