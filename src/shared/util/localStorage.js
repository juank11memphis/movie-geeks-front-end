export const removeToken = () => localStorage.removeItem('movieGeeksToken')

export const getToken = () => localStorage.getItem('movieGeeksToken')

export const getResetPasswordToken = () =>
  localStorage.getItem('resetPasswordToken')

const setToken = (token, tokenName) => {
  if (token) {
    localStorage.setItem(tokenName, token)
  } else {
    removeToken()
  }
}

export const storeToken = token => {
  setToken(token, 'movieGeeksToken')
  localStorage.removeItem('resetPasswordToken')
}

export const storeResetPasswordToken = token =>
  setToken(token, 'resetPasswordToken')

export const setTestResponseMode = responseMode =>
  localStorage.setItem('x-test-response-mode', responseMode)

export const getTestResponseMode = () =>
  localStorage.getItem('x-test-response-mode')

export const setTestIsLoggedIn = isLoggedIn =>
  localStorage.setItem('x-test-logged-in', isLoggedIn)

export const getTestIsLoggedIn = () => localStorage.getItem('x-test-logged-in')

export const setTestServiceDataOverride = data =>
  localStorage.setItem('x-test-service-data-override', JSON.stringify(data))

export const getTestServiceDataOverride = () =>
  localStorage.getItem('x-test-service-data-override')
