export const ADMIN_ROLES = ['admins']

export const HEADER_OPTIONS = [
  {
    key: 1,
    text: 'Sign Up',
    link: '/signup',
    dataTestId: 'signup-link',
  },
  {
    key: 2,
    text: 'Sign In',
    link: '/signin',
    dataTestId: 'signin-link',
  },
]
