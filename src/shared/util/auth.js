import { get } from 'lodash'

import {
  getToken,
  getResetPasswordToken,
  removeToken,
  setTestIsLoggedIn,
} from './localStorage'
import { ADMIN_ROLES } from './constants'

export function getBearerToken() {
  const token = getToken()
  return token ? `bearer ${token}` : null
}

export function getBearerResetPasswordToken() {
  const token = getResetPasswordToken()
  return token ? `bearer ${token}` : null
}

export function hasSomeRole(user, rolesToFind) {
  const userRoles = get(user, 'roles', [])
  return userRoles.some(userRole => rolesToFind.includes(userRole.name))
}

export function getAllPermissions(user) {
  const userRoles = get(user, 'roles', [])
  const allPermissions = []
  userRoles.forEach(role => {
    const rolePermissions = get(role, 'permissions', [])
    rolePermissions.forEach(permission => {
      if (!allPermissions.includes(permission)) {
        allPermissions.push(permission)
      }
    })
  })
  return allPermissions
}

export function hasSomePermission(user, permissionsToFind) {
  const userPermissions = getAllPermissions(user)
  return userPermissions.some(userPermission =>
    permissionsToFind.includes(userPermission),
  )
}

export function isAdmin(user) {
  return hasSomeRole(user, ADMIN_ROLES)
}

export function signOut() {
  removeToken()
  setTestIsLoggedIn(false)
  window.location.href = '/'
}
