import { useState } from 'react'

export default (firstValue, secondValue) => {
  const [value, setValue] = useState(firstValue)
  const toggle = () => {
    setValue(value === firstValue ? secondValue : firstValue)
  }
  return [value, toggle]
}
