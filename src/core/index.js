import configureApolloClient from './apollo/configureApolloClient'
import store from './store/store'

export { configureApolloClient, store }
