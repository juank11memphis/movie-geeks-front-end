import { action } from 'easy-peasy'

export default {
  user: null,
  fetched: false,
  setUser: action((state, user) => ({
    ...state,
    user,
    fetched: true,
  })),
}
