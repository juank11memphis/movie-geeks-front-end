import { action } from 'easy-peasy'

export default {
  query: null,
  setQuery: action((state, query) => ({
    ...state,
    query,
  })),
}
