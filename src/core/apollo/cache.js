import { InMemoryCache } from 'apollo-cache-inmemory/lib'

const dataIdFromObject = result => {
  // eslint-disable-next-line no-underscore-dangle
  if (result.id && result.__typename) {
    return result.__typename + result.id // eslint-disable-line no-underscore-dangle
  }
  return null
}

export default new InMemoryCache({
  dataIdFromObject,
  addTypename: true,
  cacheRedirects: {},
})
