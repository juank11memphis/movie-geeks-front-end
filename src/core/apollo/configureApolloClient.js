import { ApolloClient } from 'apollo-boost'

import cache from './cache'
import link from './link'

export default function() {
  const apolloClient = new ApolloClient({
    link,
    cache: cache.restore(window.__APOLLO_CLIENT__), // eslint-disable-line no-underscore-dangle
  })
  return apolloClient
}
