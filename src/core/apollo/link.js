import { setContext } from 'apollo-link-context'
import { createHttpLink } from 'apollo-link-http'
import { ApolloLink } from 'apollo-link'

import { authUtil, localStorageUtil } from '../../shared/util'

const httpLink = createHttpLink({ uri: '/graphql' })

const getTestHeaders = () => ({
  'x-test-response-mode': localStorageUtil.getTestResponseMode(),
  'x-test-logged-in': localStorageUtil.getTestIsLoggedIn(),
  'x-test-service-data-override': localStorageUtil.getTestServiceDataOverride(),
})

const contextLink = setContext(() => {
  let testHeaders = {}
  if (process.env.NODE_ENV === 'test') {
    testHeaders = getTestHeaders()
  }
  return {
    headers: {
      authorization: authUtil.getBearerToken(),
      'reset-password-token': authUtil.getBearerResetPasswordToken(),
      ...testHeaders,
    },
  }
})

const handleAuthTokenLink = new ApolloLink((operation, forward) =>
  forward(operation).map(response => {
    const {
      data: { auth },
    } = response
    if (auth && auth.token) {
      localStorageUtil.storeToken(auth.token)
    }
    return response
  }),
)

const link = ApolloLink.from([contextLink, handleAuthTokenLink, httpLink])

export default link
