import React from 'react'
import PropTypes from 'prop-types'
import Chip from '@material-ui/core/Chip'

import { useToggle } from '../../shared/hooks'

const ToggleableChip = ({ label, onToggle, className, value, ...rest }) => {
  const [isSelected, toggle] = useToggle(false, true)
  if (isSelected !== value) {
    toggle()
  }
  return (
    <Chip
      className={className}
      label={label}
      clickable
      color={isSelected ? 'primary' : 'default'}
      onClick={() => {
        toggle()
        onToggle(!isSelected, label)
      }}
      {...rest}
    />
  )
}

ToggleableChip.defaultProps = {
  value: false,
  className: null,
}

ToggleableChip.propTypes = {
  label: PropTypes.string.isRequired,
  onToggle: PropTypes.func.isRequired,
  className: PropTypes.string,
  value: PropTypes.bool,
}

export default React.memo(ToggleableChip)
