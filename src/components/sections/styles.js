import {
  defaultBoxShadow,
  mediaQueryMobile,
  mediaQueryTablet,
} from '../../styles/main'

export default {
  raised: {
    marginTop: -110,
    marginLeft: 250,
    marginRight: 250,
    marginBottom: 20,
    borderRadius: '10px',
    zIndex: 3,
    position: 'relative',
    padding: '20px 20px 70px',
    background: 'linear-gradient(0deg, #E5E5E5 0%, #fff 100%)',
    boxShadow: defaultBoxShadow,
    [mediaQueryMobile]: {
      marginLeft: 20,
      marginRight: 20,
    },
    [mediaQueryTablet]: {
      marginLeft: 20,
      marginRight: 20,
    },
  },
}
