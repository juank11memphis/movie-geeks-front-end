import React from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import classnames from 'classnames'

import styles from './styles'

const RaisedSection = ({ classes, children, className, ...rest }) => (
  <div className={classnames(className, classes.raised)} {...rest}>
    {children}
  </div>
)

RaisedSection.defaultProps = {
  className: null,
}

RaisedSection.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
}

export default withStyles(styles)(RaisedSection)
