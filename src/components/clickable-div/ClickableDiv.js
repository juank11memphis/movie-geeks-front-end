import React from 'react'
import PropTypes from 'prop-types'

const ClickableDiv = ({ children, className, onClick, style, ...rest }) => (
  <div
    className={className}
    tabIndex={0}
    role="button"
    onClick={onClick}
    onKeyDown={onClick}
    style={{
      ...style,
      outline: 'none',
    }}
    {...rest}
  >
    {children}
  </div>
)

ClickableDiv.defaultProps = {
  children: null,
  className: null,
  style: null,
}

ClickableDiv.propTypes = {
  onClick: PropTypes.func.isRequired,
  children: PropTypes.node,
  className: PropTypes.string,
  style: PropTypes.object,
}

export default ClickableDiv
