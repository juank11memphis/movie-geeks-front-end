import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import Divider from '@material-ui/core/Divider'

import styles from './styles'
import Box from '../box/Box'
import Text from '../text/Text'

const DividerComp = ({ text, classes, className }) =>
  text ? (
    <Box className={className} direction="row" align="center">
      <Divider className={classes.dividers} />
      <Text>{text}</Text>
      <Divider className={classes.dividers} />
    </Box>
  ) : (
    <Divider className={className} />
  )

DividerComp.defaultProps = {
  text: null,
  className: null,
}

DividerComp.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  text: PropTypes.string,
}

export default withStyles(styles)(DividerComp)
