export default {
  dividers: {
    flex: 1,
    marginLeft: 25,
    marginRight: 25,
  },
}
