import React from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import classnames from 'classnames'

import styles from './styles'

const Text = ({
  as,
  children,
  classes,
  color,
  className,
  bold,
  align,
  clearMargins,
  tooltip,
  ...rest
}) => {
  const ElementType = as
  const textClasses = classnames(
    className,
    classes.root,
    classes[ElementType],
    classes[color],
    classes[align],
    { [classes.bold]: bold },
    { [classes.noMargins]: clearMargins },
  )
  return (
    <ElementType className={textClasses} {...rest}>
      <span title={tooltip}>{children}</span>
    </ElementType>
  )
}

Text.defaultProps = {
  as: 'span',
  color: 'black',
  className: null,
  bold: false,
  align: 'left',
  clearMargins: false,
  tooltip: null,
}

Text.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  as: PropTypes.string,
  color: PropTypes.oneOf([
    'white',
    'black',
    'darkGrey',
    'lightGrey',
    'red',
    'blue',
  ]),
  align: PropTypes.oneOf(['left', 'center', 'right']),
  bold: PropTypes.bool,
  clearMargins: PropTypes.bool,
  tooltip: PropTypes.string,
}

export default withStyles(styles)(Text)
