import { h1, h2, h3, h5 } from '../../styles/main'
import colors from '../../styles/colors'

export default {
  root: {
    textDecoration: 'none !important',
  },
  h1: {
    ...h1,
  },
  h2: {
    ...h2,
  },
  h3: {
    ...h3,
  },
  h5: {
    ...h5,
  },
  white: {
    color: colors.white,
  },
  black: {
    color: colors.black,
  },
  darkGrey: {
    color: colors.darkGrey,
  },
  lightGrey: {
    color: colors.lightGrey,
  },
  red: {
    color: colors.red,
  },
  blue: {
    color: colors.blue,
  },
  bold: {
    fontWeight: 600,
  },
  left: {
    textAlign: 'left',
  },
  center: {
    textAlign: 'center',
  },
  right: {
    textAlign: 'right',
  },
  noMargins: {
    margin: 0,
  },
}
