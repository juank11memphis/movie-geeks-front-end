import React from 'react'
import PropTypes from 'prop-types'

const ConditionalRender = ({ children, condition, fallback }) => {
  if (condition) {
    return children
  }
  return fallback
}

ConditionalRender.defaultProps = {
  fallback: null,
}

ConditionalRender.propTypes = {
  condition: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  fallback: PropTypes.node,
}

export default React.memo(ConditionalRender)
