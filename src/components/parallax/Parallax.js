import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'

import styles from './styles'

const useParallax = initialValue => {
  const [parallax, set] = useState(`translate3d(0, ${initialValue}px, 0)`)
  const setParallax = value => set(`translate3d(0, ${value}px, 0)`)
  return [parallax, setParallax]
}

const Parallax = ({ children, image, classes, ...rest }) => {
  const [parallax, setParallax] = useParallax(window.pageYOffset / 3)

  useEffect(() => {
    const resetTransform = () => setParallax(window.pageYOffset / 3)
    setParallax(window.pageYOffset / 3)
    window.addEventListener('scroll', resetTransform)
    return () => {
      window.removeEventListener('scroll', resetTransform)
    }
  }, [window.pageYOffset])

  return (
    <div
      className={classes.root}
      style={{
        backgroundImage: `url(${image})`,
        transform: parallax,
      }}
      {...rest}
    >
      {children}
    </div>
  )
}

Parallax.defaultProps = {
  children: null,
  image: null,
}

Parallax.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.node,
  image: PropTypes.string,
}

export default withStyles(styles)(React.memo(Parallax))
