import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'

import LinkButton from '../link-button/LinkButton'
import Image from '../image/Image'

import logoImage from '../../styles/img/logo.svg'
import logoNoTextImage from '../../styles/img/logoNoText.png'
import styles from './styles'

const Brand = ({ classes }) => {
  const Logo = (
    <Image
      className={classes.brandLogoImage}
      image={logoImage}
      width={150}
      height={80}
      backgroundSize="contain"
    />
  )
  const LogoNoText = (
    <Image
      className={classes.brandLogoNoTextImage}
      image={logoNoTextImage}
      width={80}
      height={80}
      backgroundSize="contain"
    />
  )
  return (
    <Fragment>
      <LinkButton
        className={classes.brand}
        linkTo="/"
        icon={Logo}
        data-testid="brand-link"
      />
      <LinkButton
        className={classes.brandNoText}
        linkTo="/"
        icon={LogoNoText}
        data-testid="brand-link-mobile"
      />
    </Fragment>
  )
}

Brand.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Brand)
