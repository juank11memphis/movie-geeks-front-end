import React, { Fragment, useEffect } from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import { useStoreActions, useStoreState } from 'easy-peasy'
import { withRouter } from 'react-router-dom'

import Brand from './Brand'
import OptionsDesktop from './desktop/OptionsDesktop'
import OptionsMobile from './mobile/OptionsMobile'
import Box from '../box/Box'
import { routerUtil } from '../../shared/util'
import GlobalSearch from './global-search/GlobalSearch'
import LinkButton from '../link-button/LinkButton'

import styles from './styles'

const Header = ({ classes, history, location }) => {
  const currentUser = useStoreState(state => state.currentUser.user)
  const setSearchQuery = useStoreActions(dispatch => dispatch.search.setQuery)
  const currentPath = routerUtil.getCurrentPath(location)
  useEffect(() => {
    const query = routerUtil.getQueryStringParam('query')
    if (currentPath === 'search' && query) {
      setSearchQuery(query)
    }
  })
  const onSearch = searchValue => {
    history.push({
      pathname: `/search`,
      search: `?query=${searchValue}`,
    })
    setSearchQuery(searchValue)
  }
  return (
    <AppBar
      className={classes.headerTransparent}
      data-testid="header"
      position="absolute"
    >
      <Toolbar className={classes.container}>
        <Fragment>
          <Brand />
          <Box
            className={classes.optionsContainer}
            direction="row"
            align="right"
          >
            <GlobalSearch onSearch={onSearch} />
            {currentUser && (
              <LinkButton
                data-testid="discover-link"
                className={classes.discoverLink}
                textClassName={classes.discoverLinkText}
                text="Discover"
                linkTo="/discover"
              />
            )}
            <OptionsDesktop />
            <OptionsMobile />
          </Box>
        </Fragment>
      </Toolbar>
    </AppBar>
  )
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}

export default withStyles(styles)(withRouter(React.memo(Header)))
