import { defaultButton } from '../../../styles/main'
import colors from '../../../styles/colors'

export default {
  drawerList: {
    width: '250px',
  },
  openDrawerButtonContainer: {
    backgroundColor: colors.red,
    borderRadius: 4,
    width: 33,
    height: 33,
    marginRight: 10,
    padding: 5,
    boxSizing: 'border-box',
    marginLeft: 0,
  },
  openDrawerButton: {
    ...defaultButton,
    fontSize: '23px',
    color: `${colors.white} !important`,
    padding: 0,
  },
  drawerLink: {
    flex: 1,
    display: 'flex',
    textDecoration: 'none !important',
  },
}
