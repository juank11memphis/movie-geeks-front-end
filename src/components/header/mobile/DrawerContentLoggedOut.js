import React from 'react'
import List from '@material-ui/core/List'

import { Constants } from '../../../shared/util'
import HeaderDrawerItem from './HeaderDrawerItem'

const DrawerContentLoggedIn = () => (
  <List>
    {Constants.HEADER_OPTIONS.map(optionItem => (
      <HeaderDrawerItem key={optionItem.key} item={optionItem} />
    ))}
  </List>
)

export default React.memo(DrawerContentLoggedIn)
