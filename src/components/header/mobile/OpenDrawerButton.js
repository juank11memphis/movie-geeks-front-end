import React from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import IconButton from '@material-ui/core/IconButton'
import PersonIcon from '@material-ui/icons/Person'

import styles from './styles'

const OpenDrawerButton = ({ classes, onClick, ...rest }) => (
  <div className={classes.openDrawerButtonContainer}>
    <IconButton
      aria-label="Open Drawer"
      className={classes.openDrawerButton}
      onClick={onClick}
      {...rest}
    >
      <PersonIcon fontSize="inherit" />
    </IconButton>
  </div>
)

OpenDrawerButton.propTypes = {
  classes: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default withStyles(styles)(OpenDrawerButton)
