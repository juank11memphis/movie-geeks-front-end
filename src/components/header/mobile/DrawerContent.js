import React from 'react'
import { useStoreState } from 'easy-peasy'

import DrawerContentLoggedIn from './DrawerContentLoggedIn'
import DrawerContentLoggedOut from './DrawerContentLoggedOut'

const DrawerContent = () => {
  const currentUser = useStoreState(state => state.currentUser.user)
  return currentUser ? <DrawerContentLoggedIn /> : <DrawerContentLoggedOut />
}

export default React.memo(DrawerContent)
