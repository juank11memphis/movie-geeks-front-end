import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import Drawer from '@material-ui/core/Drawer'
import withStyles from '@material-ui/core/styles/withStyles'

import styles from './styles'
import OpenDrawerButton from './OpenDrawerButton'
import DrawerContent from './DrawerContent'
import ClickableDiv from '../../clickable-div/ClickableDiv'
import { useToggle } from '../../../shared/hooks'

const HeaderDrawer = ({ classes }) => {
  const [isOpen, toggleDrawer] = useToggle(false, true)
  return (
    <Fragment>
      <OpenDrawerButton
        onClick={toggleDrawer}
        data-testid="open-drawer-button"
      />
      <Drawer
        anchor="right"
        open={isOpen}
        onClose={toggleDrawer}
        data-testid="drawer"
      >
        <ClickableDiv className={classes.drawerList} onClick={toggleDrawer}>
          <DrawerContent />
        </ClickableDiv>
      </Drawer>
    </Fragment>
  )
}

HeaderDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(HeaderDrawer))
