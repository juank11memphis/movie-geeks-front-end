import React from 'react'
import Hidden from '@material-ui/core/Hidden'

import HeaderDrawer from './HeaderDrawer'

const OptionsMobile = () => (
  <Hidden mdUp>
    <HeaderDrawer />
  </Hidden>
)

export default OptionsMobile
