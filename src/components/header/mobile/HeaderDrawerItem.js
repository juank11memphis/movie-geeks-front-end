import React from 'react'
import PropTypes from 'prop-types'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core'

import styles from './styles'

const HeaderDrawerItem = ({ classes, item }) => (
  <ListItem button>
    <Link
      to={item.link}
      className={classes.drawerLink}
      data-testid={item.dataTestId}
    >
      {item.icon ? <ListItemIcon>{item.icon}</ListItemIcon> : null}
      <ListItemText primary={item.text} />
    </Link>
  </ListItem>
)

HeaderDrawerItem.propTypes = {
  item: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(HeaderDrawerItem))
