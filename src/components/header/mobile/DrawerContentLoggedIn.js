import React from 'react'
import PropTypes from 'prop-types'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import { useStoreState, useStoreActions } from 'easy-peasy'
import { withStyles } from '@material-ui/core'
import { withRouter } from 'react-router-dom'
import queryString from 'query-string'

import styles from './styles'
import { authUtil } from '../../../shared/util'

const DrawerContentLoggedIn = ({ history }) => {
  const currentUser = useStoreState(state => state.currentUser.user)
  const storeFilters = useStoreState(state => state.profile.filters)
  const setProfileSubRoute = useStoreActions(
    dispatch => dispatch.profile.setSubRoute,
  )
  return (
    <List>
      <ListItem
        button
        onClick={() => {
          setProfileSubRoute('watchlist')
          const queryStringParams = queryString.stringify(storeFilters)
          history.push({
            pathname: `/profile/${currentUser.id}/watchlist`,
            search: `?${queryStringParams}`,
          })
        }}
        data-testid="watchlist-link"
      >
        Watchlist
      </ListItem>
      <ListItem
        button
        onClick={() => {
          setProfileSubRoute('collection')
          const queryStringParams = queryString.stringify(storeFilters)
          history.push({
            pathname: `/profile/${currentUser.id}/collection`,
            search: `?${queryStringParams}`,
          })
        }}
        data-testid="collection-link"
      >
        Collection
      </ListItem>
      <ListItem
        button
        onClick={() => authUtil.signOut()}
        data-testid="signout-link"
      >
        Sign Out
      </ListItem>
    </List>
  )
}

DrawerContentLoggedIn.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
}

export default withStyles(styles)(withRouter(React.memo(DrawerContentLoggedIn)))
