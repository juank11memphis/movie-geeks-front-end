import React from 'react'
import { useStoreState } from 'easy-peasy'

import OptionsDesktopLoggedOut from './OptionsDesktopLoggedOut'
import OptionsDesktopLoggedIn from './OptionsDesktopLoggedIn'

const OptionsDesktop = () => {
  const currentUser = useStoreState(state => state.currentUser.user)
  return currentUser ? <OptionsDesktopLoggedIn /> : <OptionsDesktopLoggedOut />
}

export default React.memo(OptionsDesktop)
