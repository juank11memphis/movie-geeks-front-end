import { defaultRedButton } from '../../../styles/main'

export default {
  buttons: {
    ...defaultRedButton,
    marginLeft: 10,
  },
  authButtonsText: {
    fontSize: 16,
    fontWeight: '600',
  },
  menu: {
    minWidth: 200,
  },
  menuItemsText: {
    color: 'white',
  },
}
