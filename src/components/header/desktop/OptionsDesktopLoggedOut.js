import React from 'react'
import PropTypes from 'prop-types'
import Hidden from '@material-ui/core/Hidden'
import { withStyles } from '@material-ui/core'

import { Constants } from '../../../shared/util'
import LinkButton from '../../link-button/LinkButton'

import styles from './styles'

const OptionsDesktopLoggedOut = ({ classes }) => (
  <Hidden smDown>
    <div>
      {Constants.HEADER_OPTIONS.map(optionItem => (
        <LinkButton
          className={classes.buttons}
          textClassName={classes.authButtonsText}
          key={optionItem.key}
          linkTo={optionItem.link}
          text={optionItem.text}
          icon={optionItem.icon}
          data-testid={optionItem.dataTestId}
        />
      ))}
    </div>
  </Hidden>
)

OptionsDesktopLoggedOut.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(OptionsDesktopLoggedOut))
