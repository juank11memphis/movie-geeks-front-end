import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Hidden from '@material-ui/core/Hidden'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import { useStoreState, useStoreActions } from 'easy-peasy'
import { withRouter } from 'react-router-dom'
import queryString from 'query-string'
import { get } from 'lodash'

import styles from './styles'
import colors from '../../../styles/colors'
import { authUtil } from '../../../shared/util'

const OptionsDesktopLoggedIn = ({ classes, history }) => {
  const [anchorEl, setAnchorEl] = useState(null)
  const currentUser = useStoreState(state => state.currentUser.user)
  const storeFilters = useStoreState(state => state.profile.filters)
  const setProfileSubRoute = useStoreActions(
    dispatch => dispatch.profile.setSubRoute,
  )

  const openMenu = event => setAnchorEl(event.currentTarget)
  const closeMenu = () => setAnchorEl(null)
  const onWatchlistClick = () => {
    closeMenu()
    setProfileSubRoute('watchlist')
    const queryStringParams = queryString.stringify(storeFilters)
    history.push({
      pathname: `/profile/${currentUser.id}/watchlist`,
      search: `?${queryStringParams}`,
    })
  }
  const onCollectionClick = () => {
    closeMenu()
    setProfileSubRoute('collection')
    const queryStringParams = queryString.stringify(storeFilters)
    history.push({
      pathname: `/profile/${currentUser.id}/collection`,
      search: `?${queryStringParams}`,
    })
  }
  return (
    <Hidden smDown>
      <Button
        data-testid="user-menu-button"
        className={classes.buttons}
        aria-owns={anchorEl ? 'simple-menu' : undefined}
        aria-haspopup="true"
        onClick={openMenu}
      >
        {`${get(currentUser, 'firstname', '')} ${get(
          currentUser,
          'lastname',
          '',
        )}`}
        <ExpandMoreIcon />
      </Button>
      <Menu
        id="simple-menu"
        data-testid="user-menu"
        className={classes.menu}
        anchorEl={anchorEl}
        anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
        getContentAnchorEl={null}
        open={Boolean(anchorEl)}
        onClose={closeMenu}
        PaperProps={{
          style: {
            minWidth: 150,
            backgroundColor: colors.darkGrey,
            color: colors.white,
          },
        }}
      >
        <MenuItem
          classes={{ root: classes.menuItemsText }}
          data-testid="watchlist-option"
          onClick={onWatchlistClick}
        >
          Watchlist
        </MenuItem>
        <MenuItem
          classes={{ root: classes.menuItemsText }}
          data-testid="collection-option"
          onClick={onCollectionClick}
        >
          Collection
        </MenuItem>
        <MenuItem
          classes={{ root: classes.menuItemsText }}
          data-testid="signout-option"
          onClick={() => authUtil.signOut()}
        >
          Sign Out
        </MenuItem>
      </Menu>
    </Hidden>
  )
}

OptionsDesktopLoggedIn.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
}

export default withStyles(styles)(
  withRouter(React.memo(OptionsDesktopLoggedIn)),
)
