import colors from '../../../styles/colors'
import { mediaQueryMobile } from '../../../styles/main'

export default {
  buttons: {
    cursor: 'pointer',
    color: colors.red,
  },
  searchButton: {
    cursor: 'pointer',
    color: colors.red,
    marginRight: 5,
  },
  searchContainer: {
    width: '300px',
    padding: 0,
    [mediaQueryMobile]: {
      width: '100%',
    },
  },
  searchInput: {
    color: colors.black,
    borderColor: colors.red,
  },
  underline: {
    '&:before': {
      borderBottomColor: colors.red,
    },
    '&:after': {
      borderBottomColor: colors.red,
    },
    '&:hover:before': {
      borderBottomColor: [colors.red, '!important'],
    },
  },
}
