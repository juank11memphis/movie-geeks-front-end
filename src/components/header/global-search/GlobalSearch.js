import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import ClearIcon from '@material-ui/icons/Clear'
import SearchIcon from '@material-ui/icons/Search'
import { withRouter } from 'react-router-dom'

import Box from '../../box/Box'
import { useToggle } from '../../../shared/hooks'
import { routerUtil } from '../../../shared/util'

import styles from './styles'

let typingTimer
const onSearchChange = (searchValue, searchCallback) => {
  clearTimeout(typingTimer)
  typingTimer = setTimeout(() => searchCallback(searchValue), 300)
}

const GlobalSearch = ({ classes, onClear, onSearch, history }) => {
  const [isSearch, toggleSearch] = useToggle(false, true)
  useEffect(() => {
    const unlisten = history.listen(location => {
      const currentPath = routerUtil.getCurrentPath(location)
      if (isSearch && currentPath !== 'search') {
        toggleSearch()
      }
    })
    return unlisten
  })
  return isSearch ? (
    <Box direction="row" align="center" className={classes.searchContainer}>
      <TextField
        data-testid="search-field"
        autoFocus
        className={classes.searchField}
        fullWidth
        InputProps={{
          className: classes.searchInput,
          classes: { underline: classes.underline },
        }}
        onChange={event => onSearchChange(event.target.value, onSearch)}
      />
      <ClearIcon
        className={classes.buttons}
        onClick={() => {
          toggleSearch()
          if (onClear) {
            onClear()
          }
        }}
        data-testid="clear-icon"
      />
    </Box>
  ) : (
    <SearchIcon
      className={classes.searchButton}
      onClick={toggleSearch}
      data-testid="search-icon"
    />
  )
}

GlobalSearch.defaultProps = {
  onClear: null,
}

GlobalSearch.propTypes = {
  classes: PropTypes.object.isRequired,
  onSearch: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  onClear: PropTypes.func,
}

export default withStyles(styles)(withRouter(GlobalSearch))
