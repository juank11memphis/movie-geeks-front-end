import {
  container,
  mediaQueryMobile,
  mediaQueryDesktop,
  mediaQueryTablet,
} from '../../styles/main'
import colors from '../../styles/colors'

const headerCommon = {
  paddingTop: 5,
  paddingBottom: 10,
  marginBottom: 20,
  boxSizing: 'border-box',
  minHeight: 40,
  transition: 'all 150ms ease 0s',
}

export default {
  headerTransparent: {
    ...headerCommon,
    background: 'transparent',
    boxShadow: 'none',
    color: colors.white,
  },
  headerBlack: {
    ...headerCommon,
    color: colors.white,
    backgroundColor: colors.black,
    borderRadius: '3px',
    boxShadow:
      '0 4px 18px 0px rgba(0, 0, 0, 0.12), 0 7px 10px -5px rgba(0, 0, 0, 0.15)',
  },
  container: {
    ...container,
    paddingLeft: 0,
    paddingRight: 0,
    minHeight: '50px',
    justifyContent: 'space-between',
    alignItems: 'center',
    [mediaQueryMobile]: {
      paddingLeft: 0,
      paddingRight: 0,
    },
  },
  optionsContainer: {
    flex: 1,
    alignItems: 'center',
    padding: 0,
  },
  brand: {
    fontSize: '24px',
    textDecoration: 'none !important',
    padding: 0,
    [mediaQueryMobile]: {
      display: 'none',
    },
    [mediaQueryTablet]: {
      display: 'none',
    },
  },
  brandNoText: {
    fontSize: '24px',
    textDecoration: 'none !important',
    padding: 0,
    marginLeft: 10,
    [mediaQueryDesktop]: {
      display: 'none',
    },
  },
  buttons: {
    cursor: 'pointer',
  },
  brandLogoImage: {
    outline: 'none !important',
    [mediaQueryMobile]: {
      display: 'none',
    },
    [mediaQueryTablet]: {
      display: 'none',
    },
  },
  brandLogoNoTextImage: {
    [mediaQueryDesktop]: {
      display: 'none',
    },
  },
  discoverLink: {
    marginLeft: 5,
    marginRight: 10,
    color: `${colors.red} !important`,
  },
  discoverLinkText: {
    fontSize: 16,
    color: `${colors.red} !important`,
    fontWeight: 500,
  },
}
