import React from 'react'
import PropTypes from 'prop-types'

import Text from '../text/Text'

const SuccessMessage = ({ message }) => (
  <div>
    <Text color="blue">{message}</Text>
  </div>
)

SuccessMessage.propTypes = {
  message: PropTypes.string.isRequired,
}

export default React.memo(SuccessMessage)
