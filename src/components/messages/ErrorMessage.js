import React from 'react'
import PropTypes from 'prop-types'

import Text from '../text/Text'

const graphErrorToError = error => {
  if (error.networkError) {
    return new Error(error.networkError.message)
  }
  const { graphQLErrors = [] } = error
  const firstError = graphQLErrors[0]
  return new Error(firstError ? firstError.message : '')
}

const renderSpacers = () => (
  <div>
    <br />
    <br />
  </div>
)

const ErrorMessage = ({ error, message }) => {
  let finalError = error
  if (error && error.message && error.message.includes('Graph')) {
    finalError = graphErrorToError(error)
  }
  return (
    <div data-testid="error-message-container">
      <Text color="red" bold style={{ fontSize: 14 }}>
        {message && message}
        {message && finalError && renderSpacers()}
        {finalError && finalError.message}
      </Text>
    </div>
  )
}

ErrorMessage.defaultProps = {
  error: null,
  message: null,
}

ErrorMessage.propTypes = {
  error: PropTypes.object,
  message: PropTypes.string,
}

export default React.memo(ErrorMessage)
