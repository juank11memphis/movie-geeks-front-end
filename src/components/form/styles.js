import colors from '../../styles/colors'

export default {
  inputs: {
    width: '100%',
    backgroundColor: colors.black,
    borderRadius: '4px !important',
    '&:before': {
      borderRadius: '4px !important',
    },
  },
  inputLabels: {
    color: colors.white,
    '&$cssFocused': {
      color: colors.white,
    },
    '&:before': {
      borderRadius: '4px !important',
    },
  },
}
