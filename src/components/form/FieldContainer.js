import * as React from 'react'
import classnames from 'classnames'
import { Field } from 'formik'
import PropTypes from 'prop-types'

import ErrorMessage from '../messages/ErrorMessage'

const renderError = error => <ErrorMessage message={error} />

const FieldContainer = ({ name, comp: Comp, height, ...rest }) => (
  <Field
    name={name}
    render={({ field, form }) => (
      <div
        style={{ height }}
        className={classnames('field', {
          error: form.errors[name] && form.touched[name],
        })}
      >
        <Comp field={field} {...rest} />
        {form.touched[name] &&
          form.errors[name] &&
          renderError(form.errors[name])}
      </div>
    )}
    {...rest}
  />
)

FieldContainer.defaultProps = {
  height: 80,
}

FieldContainer.propTypes = {
  name: PropTypes.string.isRequired,
  comp: PropTypes.object.isRequired,
  height: PropTypes.number,
}

export default React.memo(FieldContainer)
