import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Formik, Form } from 'formik'

import SuccessMessage from '../messages/SuccessMessage'
import ErrorMessage from '../messages/ErrorMessage'

const FormContainer = ({
  successMessageText,
  submitAction,
  resetFormOnSubmit,
  initialValues,
  onSuccess,
  children,
  className,
  validations,
  ...rest
}) => {
  const [error, setError] = useState(null)
  const [successMessage, setSuccessMessage] = useState(null)

  const submitForm = async (data, formikBag) => {
    try {
      const response = await submitAction(data)
      setError(null)
      if (successMessageText) {
        setSuccessMessage(successMessageText)
      }
      if (resetFormOnSubmit) {
        formikBag.resetForm(initialValues)
      }
      onSuccess(response)
    } catch (error) {
      setSuccessMessage(null)
      setError(error)
    }
  }

  return (
    <div className={className} data-testid={rest['data-testid']}>
      <Formik
        initialValues={initialValues}
        validationSchema={validations}
        onSubmit={submitForm}
        render={() => (
          <Form className="ui form" style={{ marginBottom: 0 }}>
            {children}
          </Form>
        )}
      />
      {error && <ErrorMessage error={error} />}
      {successMessage && <SuccessMessage message={successMessage} />}
    </div>
  )
}

FormContainer.defaultProps = {
  successMessageText: null,
  className: null,
  initialValues: null,
  validations: null,
  resetFormOnSubmit: false,
}

FormContainer.propTypes = {
  children: PropTypes.array.isRequired,
  submitAction: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  successMessageText: PropTypes.string,
  className: PropTypes.string,
  initialValues: PropTypes.object,
  validations: PropTypes.any,
  resetFormOnSubmit: PropTypes.bool,
}

export default React.memo(FormContainer)
