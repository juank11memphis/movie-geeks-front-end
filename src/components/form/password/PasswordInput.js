import React from 'react'
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField'
import VisibilityIcon from '@material-ui/icons/Visibility'
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff'
import { withStyles } from '@material-ui/core'
import classnames from 'classnames'

import styles from './styles'
import { useToggle } from '../../../shared/hooks'

const PasswordInput = ({
  field,
  placeholder,
  classes,
  labelsClassName,
  ...rest
}) => {
  const [inputType, toggleInputType] = useToggle('password', 'text')
  return (
    <div>
      <TextField
        label={placeholder}
        placeholder={placeholder}
        type={inputType}
        className={classes.inputs}
        variant="filled"
        InputLabelProps={{
          classes: {
            root: classnames(labelsClassName, classes.labels),
            focused: classes.cssFocused,
          },
        }}
        InputProps={{
          classes: {
            root: classnames(labelsClassName, classes.labels),
            focused: classes.cssFocused,
          },
        }}
        {...field}
        {...rest}
      />
      {inputType === 'password' ? (
        <VisibilityIcon className={classes.icon} onClick={toggleInputType} />
      ) : (
        <VisibilityOffIcon className={classes.icon} onClick={toggleInputType} />
      )}
    </div>
  )
}

PasswordInput.defaultProps = {
  placeholder: '',
  labelsClassName: null,
}

PasswordInput.propTypes = {
  field: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  placeholder: PropTypes.string,
  labelsClassName: PropTypes.string,
}

export default withStyles(styles)(React.memo(PasswordInput))
