import formStyles from '../styles'

export default {
  inputs: {
    ...formStyles.inputs,
    textTransform: 'none',
  },
  icon: {
    cursor: 'pointer',
    marginTop: 15,
    marginLeft: -30,
    color: 'white',
    position: 'absolute',
  },
  labels: {
    ...formStyles.inputLabels,
  },
  cssFocused: {},
}
