import formStyles from '../styles'

export default {
  inputs: {
    ...formStyles.inputs,
  },
  labels: {
    ...formStyles.inputLabels,
  },
  cssFocused: {},
}
