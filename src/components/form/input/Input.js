import React from 'react'
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField'
import { withStyles } from '@material-ui/core'
import classnames from 'classnames'

import styles from './styles'

const Input = ({
  field,
  type,
  placeholder,
  disabled,
  classes,
  className,
  labelsClassName,
  ...rest
}) => (
  <TextField
    type={type}
    label={placeholder}
    placeholder={placeholder}
    disabled={disabled}
    className={classnames(className, classes.inputs)}
    variant="filled"
    InputLabelProps={{
      classes: {
        root: classnames(labelsClassName, classes.labels),
        focused: classes.cssFocused,
      },
    }}
    InputProps={{
      classes: {
        root: classnames(labelsClassName, classes.labels),
        focused: classes.cssFocused,
      },
    }}
    {...field}
    {...rest}
  />
)

Input.defaultProps = {
  type: 'text',
  placeholder: '',
  disabled: false,
  className: null,
  labelsClassName: null,
}

Input.propTypes = {
  field: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  labelsClassName: PropTypes.string,
  className: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
}

export default withStyles(styles)(React.memo(Input))
