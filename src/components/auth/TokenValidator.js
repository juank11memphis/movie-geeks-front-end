import React from 'react'
import { useStoreActions } from 'easy-peasy'
import { useApolloClient } from 'react-apollo-hooks'

import { localStorageUtil } from '../../shared/util'
import { authQueries } from '../../shared/graphql'

const TokenValidator = () => {
  const setCurrentUser = useStoreActions(
    dispatch => dispatch.currentUser.setUser,
  )
  const apolloClient = useApolloClient()
  const validateToken = async () => {
    try {
      const { data } = await apolloClient.mutate({
        mutation: authQueries.REFRESH_TOKEN,
      })
      const { auth } = data
      const { user } = auth
      setCurrentUser(user)
    } catch (error) {
      localStorageUtil.removeToken()
      setCurrentUser(null)
    }
  }
  validateToken()
  return null
}

export default React.memo(TokenValidator)
