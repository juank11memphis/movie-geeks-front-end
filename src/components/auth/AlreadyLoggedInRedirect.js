import React from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { useStoreState } from 'easy-peasy'

const AlreadyLoggedInRedirect = ({ redirectTo }) => {
  const currentUser = useStoreState(state => state.currentUser.user)
  return currentUser ? <Redirect to={redirectTo} /> : null
}

AlreadyLoggedInRedirect.propTypes = {
  redirectTo: PropTypes.string.isRequired,
}

export default React.memo(AlreadyLoggedInRedirect)
