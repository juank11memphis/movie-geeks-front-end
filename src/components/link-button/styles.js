import { defaultButton } from '../../styles/main'

export default {
  root: {
    ...defaultButton,
    textDecoration: 'none !important',
  },
  link: {
    ...defaultButton,
    textDecoration: 'none !important',
    display: 'flex',
    alignItems: 'center',
  },
  textPlacementRight: {
    flexDirection: 'row',
  },
  textPlacementBottom: {
    flexDirection: 'column',
  },
  icon: {
    marginRight: 10,
  },
}
