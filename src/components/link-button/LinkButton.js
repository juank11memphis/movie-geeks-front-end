import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button'
import classnames from 'classnames'

import styles from './styles'

const IconButton = ({
  classes,
  linkTo,
  icon,
  text,
  className,
  textClassName,
  textPlacement,
  ...rest
}) => (
  <Button className={classnames(classes.root, className)} {...rest}>
    <Link
      to={linkTo}
      className={classnames(classes.link, {
        [classes.textPlacementRight]: textPlacement === 'right',
        [classes.textPlacementBottom]: textPlacement === 'bottom',
      })}
    >
      <Fragment>
        {icon && (
          <div
            className={classnames({
              [classes.icon]: textPlacement === 'right',
            })}
          >
            {icon}
          </div>
        )}
        <span className={textClassName}>{text}</span>
      </Fragment>
    </Link>
  </Button>
)

IconButton.defaultProps = {
  icon: null,
  className: null,
  text: '',
  textClassName: null,
  textPlacement: 'right',
}

IconButton.propTypes = {
  classes: PropTypes.object.isRequired,
  linkTo: PropTypes.string.isRequired,
  text: PropTypes.string,
  className: PropTypes.string,
  textClassName: PropTypes.string,
  icon: PropTypes.node,
  textPlacement: PropTypes.oneOf(['right', 'bottom']),
}

export default withStyles(styles)(IconButton)
