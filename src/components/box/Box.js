import React from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import classnames from 'classnames'

import styles from './styles'

import ClickableDiv from '../clickable-div/ClickableDiv'

const Box = ({
  classes,
  direction,
  align,
  wrap,
  children,
  className,
  clearMargins,
  clearPaddings,
  onClick,
  ...rest
}) => {
  const boxClasses = classnames(
    className,
    classes.root,
    classes[direction],
    classes[align],
    classes[wrap],
    { [classes.noMargins]: clearMargins },
    { [classes.noPaddings]: clearPaddings },
  )
  return (
    <ClickableDiv
      className={boxClasses}
      {...rest}
      onClick={() => onClick && onClick()}
    >
      {children}
    </ClickableDiv>
  )
}

Box.defaultProps = {
  direction: 'column',
  align: 'left',
  className: null,
  wrap: 'nowrap',
  clearMargins: false,
  clearPaddings: false,
  onClick: null,
}

Box.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  direction: PropTypes.oneOf(['row', 'column']),
  align: PropTypes.oneOf(['left', 'center', 'right']),
  wrap: PropTypes.oneOf(['nowrap', 'wrap']),
  clearMargins: PropTypes.bool,
  clearPaddings: PropTypes.bool,
  onClick: PropTypes.func,
}

export default withStyles(styles)(Box)
