export default {
  root: {
    display: 'flex',
    paddingLeft: 20,
    paddingRight: 20,
    outline: 'none',
  },
  column: {
    flexDirection: 'column',
  },
  row: {
    flexDirection: 'row',
  },
  left: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  right: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  wrap: {
    flexWrap: 'wrap',
  },
  nowrap: {
    flexWrap: 'nowrap',
  },
  noMargins: {
    margin: 0,
  },
  noPaddings: {
    padding: 0,
  },
}
