import React from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'

import Box from '../box/Box'
import Text from '../text/Text'

const LoadingSpinner = ({ loadingText }) => (
  <Box align="center">
    <CircularProgress />
    <Text>{loadingText}</Text>
  </Box>
)

LoadingSpinner.defaultProps = {
  loadingText: 'Loading....',
}

LoadingSpinner.propTypes = {
  loadingText: PropTypes.string,
}

export default React.memo(LoadingSpinner)
