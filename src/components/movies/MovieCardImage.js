import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import notAvailableImage from '../../styles/img/imageNotAvailable.jpg'
import styles from './styles'

const IMAGES_BASE_URL = 'https://image.tmdb.org/t/p/w300'

const getMovieImageUrl = movie =>
  movie.posterPath
    ? `${IMAGES_BASE_URL}/${movie.posterPath}`
    : notAvailableImage

const MovieCardImage = ({ movie, classes }) => {
  const imageUrl = getMovieImageUrl(movie)
  const cardStyle = { backgroundImage: `url(${imageUrl})` }
  return <div style={cardStyle} className={classes.movieImage} />
}

MovieCardImage.propTypes = {
  classes: PropTypes.object.isRequired,
  movie: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(MovieCardImage))
