import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import CancelIcon from '@material-ui/icons/Cancel'
import { useStoreState } from 'easy-peasy'
import { useMutation } from 'react-apollo-hooks'
import { withRouter } from 'react-router-dom'

import styles from '../styles'

import Box from '../../box/Box'
import SliderMovieRatingText from './SliderMovieRatingText'
import { userMovieMutations } from '../../../shared/graphql'
import Slider from '../../slider/Slider'

const isValidRating = ratingValue => {
  if (ratingValue !== null && ratingValue !== undefined && ratingValue >= 0) {
    return true
  }
  return false
}

const getFinalRatingValue = (localRating, ratingValue) => {
  if (localRating !== null && localRating !== undefined && localRating >= 0) {
    return localRating
  }
  return ratingValue
}

const getRemoveRatingButtonStyle = finalRatingValue => {
  const shouldShowRemoveRatingButton = isValidRating(finalRatingValue) || false
  return {
    visibility: shouldShowRemoveRatingButton ? 'inherit' : 'hidden',
    width: shouldShowRemoveRatingButton ? '20px' : '0',
  }
}

const SliderMovieRating = ({
  classes,
  rating,
  movieId,
  history,
  onMovieRated,
}) => {
  const rateMovie = useMutation(userMovieMutations.RATE_MOVIE)
  const removeMovieRating = useMutation(userMovieMutations.REMOVE_MOVIE_RATING)
  const currentUser = useStoreState(state => state.currentUser.user)
  const [localRating, setLocalRating] = useState(null)
  const finalRatingValue = getFinalRatingValue(localRating, rating)

  const onRateMovie = newRating => {
    rateMovie({ variables: { movieId, rating: newRating } })
    if (onMovieRated) {
      onMovieRated()
    }
  }
  const onRemoveMovieRating = () =>
    removeMovieRating({ variables: { movieId } })
  return (
    <Box
      className={classes.sliderRatingContainer}
      align="left"
      data-testid="slider-movie-rating"
    >
      <SliderMovieRatingText rating={finalRatingValue} />
      <Box
        className={classes.silderRatingInputContainer}
        direction="row"
        align="center"
      >
        <CancelIcon
          data-testid="remove-rating-button"
          className={classes.removeRatingIcon}
          style={getRemoveRatingButtonStyle(finalRatingValue)}
          onClick={() => {
            setLocalRating(null)
            onRemoveMovieRating()
          }}
        />
        <Slider
          data-testid="slider-input"
          defaultValue={0}
          value={finalRatingValue || 0}
          min={0}
          max={10}
          step={0.5}
          onChange={ratingValue => setLocalRating(ratingValue)}
          onAfterChange={newRating => {
            if (!currentUser) {
              history.push('/signup')
              return
            }
            onRateMovie(newRating)
          }}
        />
      </Box>
    </Box>
  )
}

SliderMovieRating.defaultProps = {
  rating: null,
  onMovieRated: null,
}

SliderMovieRating.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  movieId: PropTypes.string.isRequired,
  rating: PropTypes.number,
  onMovieRated: PropTypes.func,
}

export default withStyles(styles)(withRouter(React.memo(SliderMovieRating)))
