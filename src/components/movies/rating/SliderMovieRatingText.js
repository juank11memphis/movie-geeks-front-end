import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import styles from '../styles'

const isValidRating = ratingValue => {
  if (ratingValue !== null && ratingValue !== undefined && ratingValue >= 0) {
    return true
  }
  return false
}

const SliderMovieRatingText = ({ classes, rating }) =>
  isValidRating(rating) ? (
    <span className={classes.watchlistButtonsText}>
      Your Rating <span className={classes.ratingText}>{rating}</span>
    </span>
  ) : (
    <span className={classes.watchlistButtonsText}>Add Your Rating</span>
  )

SliderMovieRatingText.defaultProps = {
  rating: null,
}

SliderMovieRatingText.propTypes = {
  classes: PropTypes.object.isRequired,
  rating: PropTypes.number,
}

export default withStyles(styles)(React.memo(SliderMovieRatingText))
