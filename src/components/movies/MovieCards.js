import React from 'react'
import PropTypes from 'prop-types'

import Box from '../box/Box'
import MovieCard from './MovieCard'

const MovieCards = ({ userMovies, movieCardClassName }) => (
  <Box direction="row" align="center" wrap="wrap" data-testid="movie-cards">
    {(userMovies || []).map(userMovie => (
      <MovieCard
        key={userMovie.id}
        userMovie={userMovie}
        className={movieCardClassName}
        data-testid="movie-card"
      />
    ))}
  </Box>
)

MovieCards.defaultProps = {
  userMovies: [],
  movieCardClassName: null,
}

MovieCards.propTypes = {
  userMovies: PropTypes.array,
  movieCardClassName: PropTypes.string,
}

export default React.memo(MovieCards)
