import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import Box from '../box/Box'
import MovieTrailerButton from './trailers/MovieTrailerButton'

import styles from './styles'

const MovieCardTopActions = React.memo(({ classes, movie }) => (
  <Box
    className={classes.topActionsContainer}
    align="right"
    direction="row"
    clearPaddings
    clearMargins
    data-testid="movie-card-top-actions"
  >
    <MovieTrailerButton movie={movie} />
  </Box>
))

MovieCardTopActions.propTypes = {
  classes: PropTypes.object.isRequired,
  movie: PropTypes.object.isRequired,
}

export default withStyles(styles)(MovieCardTopActions)
