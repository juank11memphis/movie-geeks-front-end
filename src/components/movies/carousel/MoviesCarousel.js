import React from 'react'
import PropTypes from 'prop-types'
import Slider from 'react-slick'
import withStyles from '@material-ui/core/styles/withStyles'

import styles from '../styles'
import MovieCard from '../MovieCard'
import MoviesCarouselNextArrow from './MoviesCarouselNextArrow'
import MoviesCarouselPreviousArrow from './MoviesCarouselPreviousArrow'

const carouselOptions = {
  dots: false,
  infinite: true,
  adaptiveHeight: false,
  draggable: false,
  swipe: false,
  speed: 500,
  autoplay: false,
  slidesToShow: 4,
  slidesToScroll: 4,
  nextArrow: <MoviesCarouselNextArrow />,
  prevArrow: <MoviesCarouselPreviousArrow />,
  responsive: [
    {
      breakpoint: 1400,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      },
    },
    {
      breakpoint: 1000,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      },
    },
    {
      breakpoint: 700,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
}

const MoviesCarousel = ({ userMovies, classes }) => (
  <Slider {...carouselOptions} className={classes.carousel}>
    {userMovies.map(userMovie => (
      <MovieCard
        key={userMovie.id}
        userMovie={userMovie}
        data-testid={userMovie.movieId}
      />
    ))}
  </Slider>
)

MoviesCarousel.defaultProps = {
  userMovies: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(MoviesCarousel)
