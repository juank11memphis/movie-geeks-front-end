import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import ArrowForwardIos from '@material-ui/icons/ArrowForwardIos'
import classnames from 'classnames'

import styles from '../styles'

const MoviesCarouselNextArrow = ({ classes, className, style, onClick }) => (
  <ArrowForwardIos
    className={classnames(className, classes.nextArrow)}
    style={{ ...style }}
    onClick={onClick}
  />
)

MoviesCarouselNextArrow.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(MoviesCarouselNextArrow)
