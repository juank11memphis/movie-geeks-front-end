import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import ArrowBackIos from '@material-ui/icons/ArrowBackIos'
import classnames from 'classnames'

import styles from '../styles'

const MoviesCarouselPreviousArrow = ({
  classes,
  className,
  style,
  onClick,
}) => (
  <ArrowBackIos
    className={classnames(className, classes.previousArrow)}
    style={{ ...style }}
    onClick={onClick}
  />
)

MoviesCarouselPreviousArrow.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(MoviesCarouselPreviousArrow)
