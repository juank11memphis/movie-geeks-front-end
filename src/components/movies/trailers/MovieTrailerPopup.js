import React from 'react'
import PropTypes from 'prop-types'
import YouTube from 'react-youtube'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import MuiDialogTitle from '@material-ui/core/DialogTitle'
import withStyles from '@material-ui/core/styles/withStyles'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'

import styles from '../styles'

const DialogTitle = withStyles(styles)(props => {
  const { classes, onClose } = props
  return (
    <MuiDialogTitle disableTypography className={classes.dialogHeader}>
      <IconButton
        aria-label="Close"
        className={classes.dialogCloseButton}
        onClick={onClose}
        size="small"
      >
        <CloseIcon />
      </IconButton>
    </MuiDialogTitle>
  )
})

const MovieTrailerPopup = React.memo(
  ({ open, onClose, youtubeTrailerKey, classes }) => {
    const options = {
      height: '390',
      width: '640',
      playerVars: {
        autoplay: 1,
      },
    }
    return (
      <Dialog onClose={onClose} open={open} maxWidth="md">
        <DialogTitle onClose={onClose} />
        <DialogContent className={classes.trailerDialog}>
          <YouTube videoId={youtubeTrailerKey} opts={options} />
        </DialogContent>
      </Dialog>
    )
  },
)

MovieTrailerPopup.defaultProps = {
  open: false,
  youtubeTrailerKey: null,
}

MovieTrailerPopup.propTypes = {
  onClose: PropTypes.func.isRequired,
  youtubeTrailerKey: PropTypes.string,
  open: PropTypes.bool,
}

export default withStyles(styles)(MovieTrailerPopup)
