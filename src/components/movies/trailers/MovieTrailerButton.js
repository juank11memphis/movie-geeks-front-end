import React, { Fragment, useState } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import VideocamIcon from '@material-ui/icons/Videocam'
import { useApolloClient } from 'react-apollo-hooks'
import { get } from 'lodash'

import { movieQueries } from '../../../shared/graphql'
import MovieTrailerPopup from './MovieTrailerPopup'

import styles from '../styles'

const MovieTrailerButton = React.memo(({ classes, movie }) => {
  const [youtubeTrailerKey, setYoutubeTrailerKey] = useState(null)
  const apolloClient = useApolloClient()
  const loadMovieTrailer = async () => {
    if (movie.youtubeTrailerKey) {
      setYoutubeTrailerKey(movie.youtubeTrailerKey)
      return
    }
    const { data } = await apolloClient.query({
      query: movieQueries.GET_YOUTUBE_OFFICIAL_TRAILER_KEY,
      variables: {
        movieId: movie.id,
      },
    })
    const key = get(data, 'key')
    setYoutubeTrailerKey(key)
  }
  const onMovieTrailerClosed = () => setYoutubeTrailerKey(null)
  return (
    <Fragment>
      <VideocamIcon
        className={classes.trailerIcon}
        onClick={loadMovieTrailer}
        data-testid="trailer-button"
      />
      <MovieTrailerPopup
        open={youtubeTrailerKey !== null}
        youtubeTrailerKey={youtubeTrailerKey}
        onClose={onMovieTrailerClosed}
      />
    </Fragment>
  )
})

MovieTrailerButton.propTypes = {
  classes: PropTypes.object.isRequired,
  movie: PropTypes.object.isRequired,
}

export default withStyles(styles)(MovieTrailerButton)
