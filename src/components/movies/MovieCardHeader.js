import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import styles from './styles'

import Box from '../box/Box'
import Text from '../text/Text'
import WatchlistButtons from './watchlist/WatchlistButtons'

const MovieCardHeader = ({ classes, movie, mainRating, inWatchlist }) => {
  return (
    <Box
      className={classes.movieCardInfoTop}
      direction="row"
      align="left"
      clearPaddings
    >
      <Box clearPaddings>
        <Text
          className={classes.movieCardTitle}
          as="h3"
          align="left"
          tooltip={movie.title}
          clearMargins
        >
          {movie.title}
        </Text>
        <Text
          align="left"
          as="h5"
          className={classes.movieCardYear}
          clearMargins
        >
          {movie.releaseDate}
        </Text>
      </Box>
      <WatchlistButtons
        movie={movie}
        mainRating={mainRating}
        inWatchlist={inWatchlist}
      />
    </Box>
  )
}

MovieCardHeader.defaultProps = {
  mainRating: null,
  inWatchlist: false,
}

MovieCardHeader.propTypes = {
  movie: PropTypes.object.isRequired,
  mainRating: PropTypes.number,
  inWatchlist: PropTypes.bool,
}

export default withStyles(styles)(React.memo(MovieCardHeader))
