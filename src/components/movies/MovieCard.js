import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import classnames from 'classnames'
import { get } from 'lodash'

import styles from './styles'

import Box from '../box/Box'
import Text from '../text/Text'
import Divider from '../divider/Divider'
import SliderMovieRating from './rating/SliderMovieRating'
import MovieCardHeader from './MovieCardHeader'
import MovieCardImage from './MovieCardImage'
import MovieCardTopActions from './MovieCardTopActions'
import { moviesUtil } from '../../shared/util'

const MovieCard = ({ userMovie, classes, className, ...rest }) => {
  const { movie, comparandUser } = userMovie
  const mainRating = get(userMovie, 'rating')
  const comparandRating = get(comparandUser, 'rating')
  const inWatchlist = get(userMovie, 'saved', false)
  const movieCardClasses = classnames(className, classes.movieCard)
  return (
    <Box
      align="center"
      {...rest}
      style={{ display: 'flex' }}
      className={movieCardClasses}
    >
      <MovieCardImage movie={movie} />
      <MovieCardTopActions movie={movie} />
      <div className={classes.movieCardInfo}>
        <MovieCardHeader
          movie={movie}
          mainRating={mainRating}
          inWatchlist={inWatchlist}
        />
        <Box align="left" className={classes.movieCardControls}>
          {moviesUtil.isValidRating(comparandRating) && (
            <Fragment>
              <Text className={classes.userRatingText} align="center">
                {`User Rating: ${comparandRating}`}
              </Text>
              <Divider className={classes.divider} />
            </Fragment>
          )}
          <SliderMovieRating rating={mainRating} movieId={movie.id} />
        </Box>
      </div>
    </Box>
  )
}

MovieCard.defaultProps = {
  className: null,
}

MovieCard.propTypes = {
  userMovie: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
}

export default withStyles(styles)(React.memo(MovieCard))
