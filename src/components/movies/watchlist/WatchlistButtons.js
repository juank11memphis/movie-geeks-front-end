import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import AddToWatchlistButton from './AddToWatchlistButton'
import RemoveFromWatchlistButton from './RemoveFromWatchlistButton'
import { moviesUtil } from '../../../shared/util'

import styles from '../styles'

const getWatchlistButtonsStyle = mainRating => ({
  visibility: moviesUtil.isValidRating(mainRating) ? 'hidden' : 'inherit',
})

const WatchlistButtons = ({ movie, mainRating, inWatchlist }) => {
  const watchlistButtonsStyle = getWatchlistButtonsStyle(mainRating, movie.id)
  return inWatchlist ? (
    <RemoveFromWatchlistButton
      movieId={movie.id}
      style={watchlistButtonsStyle}
    />
  ) : (
    <AddToWatchlistButton movieId={movie.id} style={watchlistButtonsStyle} />
  )
}

WatchlistButtons.defaultProps = {
  mainRating: null,
  inWatchlist: false,
}

WatchlistButtons.propTypes = {
  classes: PropTypes.object.isRequired,
  movie: PropTypes.object.isRequired,
  mainRating: PropTypes.number,
  inWatchlist: PropTypes.bool,
}

export default withStyles(styles)(React.memo(WatchlistButtons))
