import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import { useStoreState } from 'easy-peasy'
import { useMutation } from 'react-apollo-hooks'
import { withRouter } from 'react-router-dom'

import Image from '../../image/Image'
import removeFromWatchlistImage from '../../../styles/img/watchlistButtonAdded.svg'

import styles from '../styles'

import { userMovieMutations } from '../../../shared/graphql'

const RemoveFromWatchlistButton = ({
  classes,
  movieId,
  history,
  staticContext,
  ...rest
}) => {
  const removeFromWatchlist = useMutation(
    userMovieMutations.REMOVE_FROM_WATCHLIST,
  )
  const currentUser = useStoreState(state => state.currentUser.user)

  const onRemoveFromWatchlist = async () =>
    removeFromWatchlist({ variables: { movieId } })
  return (
    <Image
      data-testid="remove-from-watchlist-button"
      image={removeFromWatchlistImage}
      width={30}
      height={25}
      backgroundSize="contain"
      className={classes.watchlistButtons}
      onClick={() => {
        if (!currentUser) {
          history.push('/signup')
        } else {
          onRemoveFromWatchlist()
        }
      }}
      {...rest}
    />
  )
}

RemoveFromWatchlistButton.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  movieId: PropTypes.string.isRequired,
}

export default withStyles(styles)(
  withRouter(React.memo(RemoveFromWatchlistButton)),
)
