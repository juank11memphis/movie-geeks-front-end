import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import { useStoreState } from 'easy-peasy'
import { useMutation } from 'react-apollo-hooks'
import { withRouter } from 'react-router-dom'

import Image from '../../image/Image'

import styles from '../styles'
import addToWatchlistImage from '../../../styles/img/watchlistButton.svg'

import { userMovieMutations } from '../../../shared/graphql'

const AddToWatchlistButton = ({
  classes,
  movieId,
  history,
  staticContext,
  ...rest
}) => {
  const addToWatchlist = useMutation(userMovieMutations.ADD_TO_WATCHLIST)
  const currentUser = useStoreState(state => state.currentUser.user)
  const onAddToWatchlist = () => addToWatchlist({ variables: { movieId } })
  return (
    <Image
      data-testid="add-to-watchlist-button"
      image={addToWatchlistImage}
      width={30}
      height={25}
      backgroundSize="contain"
      className={classes.watchlistButtons}
      onClick={() => {
        if (!currentUser) {
          history.push('/signup')
        } else {
          onAddToWatchlist()
        }
      }}
      {...rest}
    />
  )
}

AddToWatchlistButton.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  movieId: PropTypes.string.isRequired,
}

export default withStyles(styles)(withRouter(React.memo(AddToWatchlistButton)))
