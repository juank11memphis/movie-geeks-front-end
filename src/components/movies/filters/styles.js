import colors from '../../../styles/colors'
import { defaultDivider, defaultRedButton } from '../../../styles/main'

export default {
  divider: {
    ...defaultDivider,
    margin: 0,
    marginTop: 5,
    marginBottom: 5,
    width: '100%',
  },
  genreFiltersContainer: {
    justifyContent: 'center',
  },
  movieFiltersContainer: {
    marginTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
  },
  ratingsFilterContainer: {
    width: '100%',
  },
  ratingFilterTitle: {
    marginTop: 10,
  },
  ratingsFilterSlider: {
    width: 235,
  },
  sliderContainer: {
    width: '100%',
  },
  yearsFilterContainer: {
    width: '100%',
  },
  yearsSlider: {
    width: '100%',
  },
  filtersCleanerContainer: {
    padding: 0,
    marginTop: 5,
    marginBottom: 5,
  },
  filtersCleanerChip: {
    background: 'none',
    color: 'black',
  },
  filtersCleanerDeleteIcon: {
    color: colors.red,
  },
  chips: {
    borderRadius: 4,
    backgroundColor: `${colors.mediumGrey2} !important`,
    width: 92,
    height: 23,
    marginLeft: 10,
    marginBottom: 10,
    color: colors.white,
  },
  selectedChips: {
    backgroundColor: 'black !important',
  },
  yearsRangeText: {
    marginTop: 10,
    marginBottom: 10,
  },
  yearsRangeContainer: {
    width: '100%',
    boxSizing: 'border-box',
    paddingRight: 15,
    paddingLeft: 10,
  },
  ratingSliderContainer: {
    width: '100%',
    boxSizing: 'border-box',
    paddingRight: 15,
    paddingLeft: 10,
    marginTop: 10,
    marginBottom: 24,
  },
  applyButton: {
    ...defaultRedButton,
    marginTop: 15,
  },
}
