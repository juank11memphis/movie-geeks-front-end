import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import Hidden from '@material-ui/core/Hidden'

import Box from '../../box/Box'
import GenreFilters from './GenreFilters'
import RatingFilters from './RatingFilters'
import YearsFilters from './YearsFilters'

import styles from './styles'

const MovieFilters = ({
  classes,
  onChange,
  onApply,
  value,
  showMinimumRatingFilter,
}) => (
  <Box className={classes.movieFiltersContainer}>
    <GenreFilters
      value={value.genres}
      onChange={selectedGenres =>
        onChange({
          ...value,
          genres: selectedGenres,
        })
      }
    />
    {showMinimumRatingFilter && (
      <RatingFilters
        value={parseFloat(value.minimumRating)}
        onChange={minimumRating => onChange({ ...value, minimumRating })}
      />
    )}
    <YearsFilters
      value={value.yearsRange}
      onChange={yearsRange => onChange({ ...value, yearsRange })}
    />
    <Hidden mdUp>
      <Button
        data-testid="movie-filters-apply-button"
        className={classes.applyButton}
        onClick={onApply}
      >
        Apply
      </Button>
    </Hidden>
  </Box>
)

MovieFilters.defaultProps = {
  value: {
    genres: [],
    minimumRating: 0,
    yearsRange: undefined,
  },
  showMinimumRatingFilter: true,
}

MovieFilters.propTypes = {
  onChange: PropTypes.func.isRequired,
  onApply: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  value: PropTypes.shape({
    genres: PropTypes.array,
    minimumRating: PropTypes.number,
    yearsRange: PropTypes.array,
  }),
  showMinimumRatingFilter: PropTypes.bool,
}

export default withStyles(styles)(React.memo(MovieFilters))
