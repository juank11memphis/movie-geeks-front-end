import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import styles from './styles'
import Box from '../../box/Box'
import Text from '../../text/Text'
import Range from '../../slider/Range'
import Divider from '../../divider/Divider'

const getCurrentYear = () => new Date().getFullYear()
const getDefaultValue = () => [1900, getCurrentYear()]

const YearsFilter = ({ classes, onChange, value }) => {
  const [localYears, setLocalYears] = useState(null)
  const finalYears = localYears || value
  return (
    <Box className={classes.yearsFilterContainer} clearPaddings>
      <Text as="h5" color="white" clearMargins bold>
        Years Range
      </Text>
      <Divider className={classes.divider} />
      <Text
        className={classes.yearsRangeText}
        as="h5"
        color="white"
        clearMargins
      >
        {`From ${finalYears[0]} to ${finalYears[1]}`}
      </Text>
      <div className={classes.yearsRangeContainer}>
        <Range
          min={1900}
          max={getCurrentYear()}
          defaultValue={getDefaultValue()}
          value={finalYears}
          onChange={value => setLocalYears(value)}
          allowCross={false}
          onAfterChange={value => onChange(value)}
        />
      </div>
    </Box>
  )
}

YearsFilter.defaultProps = {
  value: getDefaultValue(),
}

YearsFilter.propTypes = {
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.array,
}

export default withStyles(styles)(React.memo(YearsFilter))
