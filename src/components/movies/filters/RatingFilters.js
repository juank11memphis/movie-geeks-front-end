import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import Slider from '../../slider/Slider'
import Text from '../../text/Text'
import Box from '../../box/Box'
import Divider from '../../divider/Divider'

import styles from './styles'

const RatingFilters = ({ classes, onChange, value }) => {
  const [localRating, setRating] = useState(null)
  const finalRating =
    localRating !== undefined && localRating !== null ? localRating : value
  return (
    <Box align="left" clearPaddings className={classes.ratingsFilterContainer}>
      <Box
        direction="row"
        clearMargins
        clearPaddings
        align="center"
        className={classes.ratingFilterTitle}
      >
        <Text as="h5" color="white" clearMargins bold>
          Minimum Rating&nbsp;
        </Text>
        <Text as="h3" color="white" clearMargins bold>
          {finalRating || 0}
        </Text>
      </Box>
      <Divider className={classes.divider} />
      <div className={classes.ratingSliderContainer}>
        <Slider
          defaultValue={0}
          value={finalRating || 0}
          min={0}
          max={10}
          step={0.5}
          onChange={ratingValue => setRating(ratingValue)}
          onAfterChange={newRating => {
            setRating(newRating)
            onChange(newRating)
          }}
        />
      </div>
    </Box>
  )
}

RatingFilters.defaultProps = {
  value: 0,
}

RatingFilters.propTypes = {
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.number,
}

export default withStyles(styles)(React.memo(RatingFilters))
