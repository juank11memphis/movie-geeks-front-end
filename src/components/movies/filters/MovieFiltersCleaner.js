import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import Chip from '@material-ui/core/Chip'
import { isEmpty, get } from 'lodash'

import styles from './styles'
import Box from '../../box/Box'

const MovieFiltersCleaner = ({
  classes,
  filters,
  onClearAll,
  onClearRatings,
  onClearYears,
  onClearGenres,
  showMinimumRatingCleaner,
}) => {
  const genreFilters = get(filters, 'genres')
  const minimumRatingFilter = get(filters, 'minimumRating', -1)
  const yearsRangeFilter = get(filters, 'yearsRange')
  let isFiltersEmpty = isEmpty(genreFilters) && isEmpty(yearsRangeFilter)
  if (isFiltersEmpty && showMinimumRatingCleaner && minimumRatingFilter < 0) {
    isFiltersEmpty = true
  } else if (isFiltersEmpty && !showMinimumRatingCleaner) {
    isFiltersEmpty = true
  } else {
    isFiltersEmpty = false
  }
  return (
    <Box
      className={classes.filtersCleanerContainer}
      direction="row"
      wrap="wrap"
    >
      {!isEmpty(genreFilters) && (
        <Chip
          className={classes.filtersCleanerChip}
          classes={{ deleteIcon: classes.filtersCleanerDeleteIcon }}
          data-testid="filter-cleaner-genres"
          label="Genres"
          clickable
          color="primary"
          onClick={onClearGenres}
          onDelete={onClearGenres}
        />
      )}
      {minimumRatingFilter > -1 && showMinimumRatingCleaner && (
        <Chip
          className={classes.filtersCleanerChip}
          classes={{ deleteIcon: classes.filtersCleanerDeleteIcon }}
          data-testid="filter-cleaner-ratings"
          label="Ratings"
          clickable
          color="primary"
          onClick={onClearRatings}
          onDelete={onClearRatings}
        />
      )}
      {!isEmpty(yearsRangeFilter) && (
        <Chip
          className={classes.filtersCleanerChip}
          classes={{ deleteIcon: classes.filtersCleanerDeleteIcon }}
          data-testid="filter-cleaner-years"
          label="Years Range"
          clickable
          color="primary"
          onClick={onClearYears}
          onDelete={onClearYears}
        />
      )}
      {!isFiltersEmpty && (
        <Chip
          className={classes.filtersCleanerChip}
          classes={{ deleteIcon: classes.filtersCleanerDeleteIcon }}
          data-testid="filter-cleaner-all"
          label="Clear All"
          clickable
          color="primary"
          onClick={onClearAll}
          onDelete={onClearAll}
        />
      )}
    </Box>
  )
}

MovieFiltersCleaner.defaultProps = {
  filters: null,
  showMinimumRatingCleaner: true,
}

MovieFiltersCleaner.propTypes = {
  classes: PropTypes.object.isRequired,
  onClearAll: PropTypes.func.isRequired,
  onClearGenres: PropTypes.func.isRequired,
  onClearRatings: PropTypes.func.isRequired,
  onClearYears: PropTypes.func.isRequired,
  filters: PropTypes.object,
  showMinimumRatingCleaner: PropTypes.bool,
}

export default withStyles(styles)(React.memo(MovieFiltersCleaner))
