import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { includes, reduce } from 'lodash'
import { withStyles } from '@material-ui/core'

import ToggleableChip from '../../toggleable-chip/ToggleableChip'
import Text from '../../text/Text'
import Box from '../../box/Box'
import Divider from '../../divider/Divider'

import allGenres from './allGenres'

import styles from './styles'

const valueToSelectionsMap = value => {
  if (value.length <= 0) {
    return {}
  }
  return reduce(value, (acc, genre) => ({ ...acc, [genre]: true }), {})
}

const selectionsMapToValue = map => {
  const selectedValues = Object.entries(map).filter(
    // eslint-disable-next-line
    ([genre, isSelected]) => isSelected,
  )
  return selectedValues.map(([genre]) => genre)
}

const getNewSelectionsMap = (selectionsMap, genre, isSelected) => ({
  ...selectionsMap,
  [genre]: isSelected,
})

const GenreFilters = ({ classes, onChange, value }) => {
  const [selectionsMap, setSelectionsMap] = useState(
    valueToSelectionsMap(value),
  )
  return (
    <Box className={classes.genreFiltersContainer} clearPaddings>
      <Text as="h5" color="white" clearMargins bold>
        Movie Genre
      </Text>
      <Divider className={classes.divider} />
      <Box direction="row" clearPaddings wrap="wrap">
        {allGenres.map(genre => (
          <ToggleableChip
            data-testid="genre-chip"
            className={classes.chips}
            classes={{ colorPrimary: classes.selectedChips }}
            key={genre}
            label={genre}
            value={includes(value, genre)}
            onToggle={isSelected => {
              const newMap = getNewSelectionsMap(
                selectionsMap,
                genre,
                isSelected,
              )
              setSelectionsMap(newMap)
              const newValue = selectionsMapToValue(newMap)
              onChange(newValue && newValue.length > 0 ? newValue : undefined)
            }}
          />
        ))}
      </Box>
    </Box>
  )
}

GenreFilters.defaultProps = {
  value: [],
}

GenreFilters.propTypes = {
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.array,
}

export default withStyles(styles)(React.memo(GenreFilters))
