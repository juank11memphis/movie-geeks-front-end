import colors from '../../styles/colors'
import { defaultDivider, mediaQueryMobileSuperSmall } from '../../styles/main'

export default {
  carousel: {
    width: '100%',
    height: '100%',
    userSelect: 'auto',
  },
  nextArrow: {
    color: `${colors.white} !important`,
    zIndex: 100,
  },
  previousArrow: {
    color: `${colors.white} !important`,
    zIndex: 100,
  },
  movieCard: {
    width: 300,
    minHeight: 430,
    justifyContent: 'start',
    outline: 'none !important',
    paddingLeft: 0,
    paddingRight: 0,
    boxShadow: '0px 0px 4px 0px rgba(0,0,0,0.5)',
    margin: '0 auto',
    borderRadius: 4,
    [mediaQueryMobileSuperSmall]: {
      width: 280,
    },
  },
  movieImage: {
    width: 300,
    height: 450,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    [mediaQueryMobileSuperSmall]: {
      width: 280,
    },
  },
  movieCardInfo: {
    boxSizing: 'border-box',
    width: 300,
    backgroundColor: 'white',
    padding: 5,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    [mediaQueryMobileSuperSmall]: {
      width: 280,
    },
  },
  movieCardInfoTop: {
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  movieCardControls: {
    width: '100%',
    justifyContent: 'space-between',
    flex: 1,
    paddingLeft: 0,
    paddingRight: 0,
    boxSizing: 'border-box',
  },
  movieCardTitle: {
    fontSize: '18px',
    width: 260,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    fontWeight: 300,
    [mediaQueryMobileSuperSmall]: {
      width: 140,
    },
  },
  movieCardYear: {
    fontSize: '14px',
    minHeight: 16,
  },
  buttons: {
    cursor: 'pointer',
  },
  watchlistButtons: {
    cursor: 'pointer',
    outline: 'none !important',
    minWidth: 30,
    minHeight: 25,
    [mediaQueryMobileSuperSmall]: {
      marginLeft: -20,
    },
  },
  watchlistButtonsText: {
    fontSize: 14,
  },
  userRatingText: {
    fontSize: 14,
    marginBottom: 5,
  },
  divider: {
    ...defaultDivider,
    margin: '0 !important',
  },
  ratingText: {
    fontSize: 18,
    color: colors.red,
    fontWeight: '600',
  },
  sliderRatingContainer: {
    width: '100%',
    padding: 0,
  },
  silderRatingInputContainer: {
    width: '100%',
    padding: 0,
    boxSizing: 'border-box',
    paddingRight: 9,
  },
  removeRatingIcon: {
    marginRight: 10,
    cursor: 'pointer',
    color: colors.red,
  },
  topActionsContainer: {
    width: 300,
    height: 40,
    position: 'absolute',
    background:
      'linear-gradient(to bottom, rgba(0,0,0,0.65) 0%,rgba(0,0,0,0) 100%)',
    [mediaQueryMobileSuperSmall]: {
      width: 280,
    },
  },
  trailerIcon: {
    color: colors.white,
    marginRight: 10,
    fontSize: 40,
    cursor: 'pointer',
  },
  trailerDialog: {
    backgroundColor: '#000000',
  },
  dialogCloseButton: {
    position: 'absolute',
    right: 5,
    top: 5,
    color: colors.white,
  },
  dialogHeader: {
    height: 35,
    boxSizing: 'border-box',
    backgroundColor: '#000000',
  },
}
