import React from 'react'
import PropTypes from 'prop-types'

import LoadingSpinner from '../loading-spinner/LoadingSpinner'
import ErrorMessage from '../messages/ErrorMessage'

const QueryResponseWrapper = ({ children, loading, error }) => {
  if (loading) {
    return <LoadingSpinner />
  }
  if (error) {
    return (
      <ErrorMessage error={error} message="An unexpected error has occurred" />
    )
  }
  return children
}

QueryResponseWrapper.defaultProps = {
  loading: false,
  error: null,
}

QueryResponseWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  loading: PropTypes.bool,
  error: PropTypes.object,
}

export default React.memo(QueryResponseWrapper)
