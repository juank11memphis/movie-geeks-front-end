import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import Range from 'rc-slider/lib/Range'

import colors from '../../styles/colors'
import styles from './styles'
import { defaultBoxShadow } from '../../styles/main'

const trackStyle = {
  borderRadius: 0,
  backgroundColor: colors.red,
  height: 14,
}

const railStyle = {
  height: 14,
  backgroundColor: colors.mediumGrey,
  borderRadius: 0,
}

const handleStyle = {
  backgroundColor: colors.red,
  width: 18,
  height: 18,
  borderStyle: 'none',
  marginTop: -2,
  boxShadow: defaultBoxShadow,
}

const RangeComp = ({ ...rest }) => (
  <Range
    {...rest}
    trackStyle={[trackStyle, trackStyle]}
    railStyle={railStyle}
    handleStyle={[handleStyle, handleStyle]}
  />
)

RangeComp.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(RangeComp))
