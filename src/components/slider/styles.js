import colors from '../../styles/colors'

const sliderRatingTracks = {
  height: 14,
  backgroundColor: colors.red,
  '&:hover': {
    boxShadow: 'none !important',
  },
}

export default {
  sliderRatingInput: {
    paddingLeft: 10,
  },
  sliderRatingLeftTrack: {
    ...sliderRatingTracks,
    borderTopLeftRadius: 2,
    borderBottomLeftRadius: 2,
  },
  sliderRatingRightTrack: {
    ...sliderRatingTracks,
    borderTopRightRadius: 2,
    borderBottomRightRadius: 2,
    opacity: 1,
    backgroundColor: '#bfbfbf !important',
  },
  sliderRatingThumbWrapper: {
    '&:hover': {
      boxShadow: 'none !important',
    },
  },
  sliderRatingThumb: {
    width: 18,
    height: 18,
    backgroundColor: colors.red,
    '&:hover': {
      boxShadow: 'none !important',
    },
  },
  sliderRatingActivated: {
    boxShadow: 'none !important',
    '&:hover': {
      boxShadow: 'none !important',
    },
  },
}
