import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import Slider from 'rc-slider/lib/Slider'

import colors from '../../styles/colors'
import { defaultBoxShadow } from '../../styles/main'
import styles from './styles'

let triggerAfterChange = false

const NewSlider = ({ onChange, onAfterChange, ...rest }) => (
  <Slider
    {...rest}
    onChange={ratingValue => {
      triggerAfterChange = true
      onChange(ratingValue)
    }}
    onAfterChange={newRating => {
      if (triggerAfterChange) {
        triggerAfterChange = false
        onAfterChange(newRating)
      }
    }}
    trackStyle={{
      borderRadius: 0,
      backgroundColor: colors.red,
      height: 14,
    }}
    railStyle={{
      height: 14,
      backgroundColor: colors.mediumGrey,
      borderRadius: 0,
    }}
    handleStyle={{
      backgroundColor: colors.red,
      width: 18,
      height: 18,
      borderStyle: 'none',
      marginTop: -2,
      boxShadow: defaultBoxShadow,
    }}
  />
)

NewSlider.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(NewSlider))
