import colors from '../../styles/colors'
import { mediaQueryMobile, defaultButton } from '../../styles/main'

export default {
  root: {
    borderStyle: 'solid',
    borderWidth: 0,
    borderTopWidth: 2,
    borderTopColor: colors.red,
    backgroundColor: 'black',
    height: 80,
    [mediaQueryMobile]: {
      flexDirection: 'column',
      height: 100,
    },
  },
  text: {
    [mediaQueryMobile]: {
      marginTop: 10,
    },
  },
  poweredBy: {
    position: 'absolute',
    right: 0,
    [mediaQueryMobile]: {
      position: 'relative',
      marginTop: 10,
    },
  },
  subFooter: {
    height: 85,
    backgroundColor: 'black',
  },
  buttonsContainer: {
    padding: 0,
    cursor: 'pointer',
  },
  buttons: {
    color: `${colors.white} !important`,
    outline: 'none !important',
  },
  buttonsText: {
    ...defaultButton,
    color: `${colors.white} !important`,
    textDecoration: 'none !important',
  },
}
