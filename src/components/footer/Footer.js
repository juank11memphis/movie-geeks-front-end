import React, { Fragment } from 'react'
import { withStyles } from '@material-ui/core'
import PropTypes from 'prop-types'

import Text from '../text/Text'
import Box from '../box/Box'
import Image from '../image/Image'
import packageJson from '../../../package.json'
import SubFooter from './SubFooter'

import styles from './styles'
import poweredByTheMovieDB from '../../styles/img/poweredByTheMovieDB.svg'

const getCurrentYear = () => new Date().getFullYear()

const Footer = ({ classes }) => (
  <Fragment>
    <SubFooter />
    <Box
      direction="row"
      align="center"
      className={classes.root}
      data-testid="footer"
    >
      <Text className={classes.text} as="h5" color="white" clearMargins>
        {getCurrentYear()} © MovieGeeks {packageJson.version}
      </Text>
      <Image
        className={classes.poweredBy}
        image={poweredByTheMovieDB}
        width={150}
        height={50}
        backgroundSize="contain"
      />
    </Box>
  </Fragment>
)

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Footer)
