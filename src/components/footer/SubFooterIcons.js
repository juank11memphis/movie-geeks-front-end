import React from 'react'

import Image from '../image/Image'

import homeIcon from '../../styles/img/icon-home.svg'
import signInIcon from '../../styles/img/icon-sign-in.svg'
import signUpIcon from '../../styles/img/icon-sign-up.svg'

export const HomeIcon = (
  <Image image={homeIcon} width={30} height={30} backgroundSize="contain" />
)

export const SignInIcon = (
  <Image image={signInIcon} width={30} height={30} backgroundSize="contain" />
)

export const SignUpIcon = (
  <Image image={signUpIcon} width={30} height={30} backgroundSize="contain" />
)
