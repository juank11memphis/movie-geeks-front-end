import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import { useStoreState } from 'easy-peasy'

import styles from './styles'

import Box from '../box/Box'
import SignedOutButtons from './SignedOutButtons'
import SignedInButtons from './SignedInButtons'

const SubFooter = ({ classes }) => {
  const currentUser = useStoreState(state => state.currentUser.user)
  return (
    <Box
      align="center"
      direction="row"
      className={classes.subFooter}
      data-testid="subFooter"
    >
      {!currentUser ? <SignedOutButtons /> : <SignedInButtons />}
    </Box>
  )
}

SubFooter.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(SubFooter))
