import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import { useStoreState, useStoreActions } from 'easy-peasy'
import queryString from 'query-string'
import { withRouter } from 'react-router-dom'

import styles from './styles'
import collectionIcon from '../../styles/img/icon-collection.svg'
import watchlistIcon from '../../styles/img/icon-watchlist.svg'

import LinkButton from '../link-button/LinkButton'
import Box from '../box/Box'
import Text from '../text/Text'
import Image from '../image/Image'
import { HomeIcon } from './SubFooterIcons'

const SignedInButtons = ({ classes, history }) => {
  const currentUser = useStoreState(state => state.currentUser.user)
  const storeFilters = useStoreState(state => state.profile.filters)
  const setProfileSubRoute = useStoreActions(
    dispatch => dispatch.profile.setSubRoute,
  )
  return (
    <Fragment>
      <LinkButton
        className={classes.buttons}
        text="Home"
        textPlacement="bottom"
        linkTo="/"
        icon={HomeIcon}
        style={{ marginRight: 40, padding: 0 }}
        data-testid="subfooter-signedin-home-link"
      />
      <Box
        className={classes.buttonsContainer}
        align="center"
        direction="column"
        style={{ marginRight: 40, padding: 0 }}
        onClick={() => {
          setProfileSubRoute('watchlist')
          const queryStringParams = queryString.stringify(storeFilters)
          history.push({
            pathname: `/profile/${currentUser.id}/watchlist`,
            search: `?${queryStringParams}`,
          })
        }}
        data-testid="subfooter-watchlist-link"
      >
        <Image
          image={watchlistIcon}
          width={30}
          height={30}
          backgroundSize="contain"
        />
        <Text className={classes.buttonsText} color="white">
          Watchlist
        </Text>
      </Box>
      <Box
        align="center"
        direction="column"
        className={classes.buttonsContainer}
        style={{ padding: 0 }}
        onClick={() => {
          setProfileSubRoute('collection')
          const queryStringParams = queryString.stringify(storeFilters)
          history.push({
            pathname: `/profile/${currentUser.id}/collection`,
            search: `?${queryStringParams}`,
          })
        }}
        data-testid="subfooter-collection-link"
      >
        <Image
          image={collectionIcon}
          width={30}
          height={30}
          backgroundSize="contain"
        />
        <Text className={classes.buttonsText} color="white">
          Collection
        </Text>
      </Box>
    </Fragment>
  )
}

SignedInButtons.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
}

export default withStyles(styles)(withRouter(React.memo(SignedInButtons)))
