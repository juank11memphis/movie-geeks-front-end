import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

import styles from './styles'

import LinkButton from '../link-button/LinkButton'
import { HomeIcon, SignInIcon, SignUpIcon } from './SubFooterIcons'

const SignedOutButtons = ({ classes }) => (
  <Fragment>
    <LinkButton
      className={classes.buttons}
      text="Home"
      textPlacement="bottom"
      linkTo="/"
      icon={HomeIcon}
      style={{ marginRight: 40 }}
      data-testid="subfooter-home-link"
    />
    <LinkButton
      className={classes.buttons}
      text="Sign In"
      textPlacement="bottom"
      linkTo="signin"
      icon={SignInIcon}
      style={{ marginRight: 40 }}
      data-testid="subfooter-signin-link"
    />
    <LinkButton
      className={classes.buttons}
      text="Sign Up"
      textPlacement="bottom"
      linkTo="signup"
      icon={SignUpIcon}
      data-testid="subfooter-signup-link"
    />
  </Fragment>
)

SignedOutButtons.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(React.memo(SignedOutButtons))
