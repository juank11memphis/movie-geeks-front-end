import React from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import Avatar from '@material-ui/core/Avatar'
import classnames from 'classnames'

import styles from './styles'

const RaisedAvatar = ({ classes, className, ...rest }) => {
  const avatarClasses = classnames(className, classes.root)
  return <Avatar {...rest} className={avatarClasses} />
}

RaisedAvatar.defaultProps = {
  className: null,
}

RaisedAvatar.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
}

export default withStyles(styles)(RaisedAvatar)
