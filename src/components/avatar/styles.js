import { defaultBoxShadow } from '../../styles/main'

export default {
  root: {
    width: 200,
    height: 200,
    boxShadow: defaultBoxShadow,
  },
}
