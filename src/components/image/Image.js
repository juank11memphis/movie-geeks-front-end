import React from 'react'
import PropTypes from 'prop-types'

import ClickableDiv from '../clickable-div/ClickableDiv'

const Image = ({
  image,
  width,
  height,
  backgroundSize,
  className,
  style,
  onClick,
  ...rest
}) => (
  <ClickableDiv
    className={className}
    style={{
      ...style,
      backgroundSize,
      backgroundRepeat: 'no-repeat',
      backgroundImage: `url(${image})`,
      backgroundPosition: 'center',
      width,
      height,
    }}
    onClick={onClick}
    {...rest}
  />
)

Image.defaultProps = {
  backgroundSize: 'cover',
  className: null,
  style: null,
  width: null,
  height: null,
  onClick: () => {},
}

Image.propTypes = {
  image: PropTypes.string.isRequired,
  width: PropTypes.number,
  height: PropTypes.number,
  backgroundSize: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
  onClick: PropTypes.func,
}

export default Image
