import grey from '@material-ui/core/colors/grey'

import {
  defaultBackgroundImage,
  defaultDivider,
  container,
} from '../../styles/main'

export default {
  root: {
    height: 700,
    color: grey['50'],
    textAlign: 'center',
    alignItems: 'flex-start',
    ...defaultBackgroundImage,
    paddingLeft: 50,
    paddingRight: 50,
  },
  container: {
    ...container,
  },
  divider: {
    ...defaultDivider,
    margin: 0,
  },
}
