import React from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'

import styles from './styles'
import Text from '../text/Text'
import Box from '../box/Box'
import Divider from '../divider/Divider'

const Banner = ({
  classes,
  image,
  mainText,
  mainTextClassName,
  secondaryText,
  ...rest
}) => (
  <Box
    align="center"
    className={classes.root}
    style={{
      backgroundImage: `url(${image})`,
    }}
    {...rest}
  >
    <div className={classes.container}>
      <Text
        as="h2"
        color="black"
        align="left"
        clearMargins
        className={mainTextClassName}
      >
        {mainText}
      </Text>
      {secondaryText && (
        <Text as="h3" color="white" align="center">
          {secondaryText}
        </Text>
      )}
      <Divider className={classes.divider} />
    </div>
  </Box>
)

Banner.defaultProps = {
  secondaryText: '',
  mainTextClassName: null,
}

Banner.propTypes = {
  classes: PropTypes.object.isRequired,
  image: PropTypes.string.isRequired,
  mainText: PropTypes.string.isRequired,
  secondaryText: PropTypes.string,
  mainTextClassName: PropTypes.string,
}

export default withStyles(styles)(Banner)
