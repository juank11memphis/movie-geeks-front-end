import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { hot } from 'react-hot-loader'
import { StoreProvider } from 'easy-peasy'
import { ApolloProvider as ApolloHooksProvider } from 'react-apollo-hooks'

import AppRoutes from './AppRoutes'
import { configureApolloClient, store } from './core'
import { TokenValidator, Header } from './components'

const apolloClient = configureApolloClient()

const AppMain = () => (
  <ApolloHooksProvider client={apolloClient}>
    <BrowserRouter>
      <StoreProvider store={store}>
        <TokenValidator />
        <Header />
        <AppRoutes />
      </StoreProvider>
    </BrowserRouter>
  </ApolloHooksProvider>
)

export default hot(module)(AppMain)
