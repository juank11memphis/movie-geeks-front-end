import React from 'react'
import ReactDom from 'react-dom'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import 'rc-slider/assets/index.css'

import registerServiceWorker, { unregister } from './registerServiceWorker'

import App from './App'

ReactDom.render(<App />, document.getElementById('app'))
if (process.env.NODE_ENV === 'production') {
  registerServiceWorker()
} else {
  unregister()
}
