#!/usr/bin/env bash
ssh juanca@104.248.62.212 <<EOF
 cd movie-geeks-front-end
 git checkout master
 git reset --hard
 git stash
 git pull --rebase
 make deploy-prod
 exit
EOF
