#!/usr/bin/env bash
if [ "$ENVIRONMENT" = "production" ]
then
  yarn build
  yarn start:prod
fi
